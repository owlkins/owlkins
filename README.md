# Owlkins

v1.0.1

Self-managed open-source website for your baby registry, photos, and tracking.

## Installation
Installation of Owlkins is a lengthy process that can be found at the [Install](https://owlkins.com/documentation/install/) link.

## Support
The project's Gitlab Issues and Wiki are a growing helpful resource in addition to
the [Owlkins Website](https://owlkins.com)

## Roadmap & Contributing

We welcome contributions to Owlkins! See out [Contribution Page](https://owlkins.com/info/contribute)

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
Owlkins is [licensed](https://owlkins.com/legal/license) under "MIT License"

