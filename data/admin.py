from django.contrib import admin
from .models import Sleep, Feeding, Poop

MOVE_TO_END = [
    'extra'
]

DONT_SHOW = [
    'created_at',
    'updated_at',
]

class SleepAdmin(admin.ModelAdmin):
    list_display = [each.name for each in Sleep._meta.fields if each.name not in MOVE_TO_END + DONT_SHOW] + ['length'] + MOVE_TO_END
    readonly_fields = ('length',)

class FeedingAdmin(admin.ModelAdmin):
    list_display = [each.name for each in Feeding._meta.fields if each.name not in MOVE_TO_END + DONT_SHOW] + ['amount_in_ounces'] + MOVE_TO_END
    readonly_fields = ('amount_in_ounces',)

class PoopAdmin(admin.ModelAdmin):
    list_display = [each.name for each in Poop._meta.fields if each.name not in MOVE_TO_END + DONT_SHOW] + MOVE_TO_END
    # readonly_fields = ('',)

admin.site.register(Sleep, SleepAdmin)
admin.site.register(Feeding, FeedingAdmin)
admin.site.register(Poop, PoopAdmin)