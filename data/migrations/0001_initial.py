# Generated by Django 3.1.3 on 2020-12-01 16:46

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Sleep',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateTimeField()),
                ('end', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('extra', models.CharField(blank=True, help_text='any notes or other info to record', max_length=1024, null=True)),
                ('type', models.CharField(choices=[('nap', 'nap'), ('overnight', 'overnight')], max_length=15)),
                ('temperature', models.IntegerField(blank=True, help_text='°F', null=True)),
                ('entered_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Feeding',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateTimeField()),
                ('end', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('extra', models.CharField(blank=True, help_text='any notes or other info to record', max_length=1024, null=True)),
                ('type', models.CharField(choices=[('breast', 'breast'), ('bottle-breastmilk', 'bottle-breastmilk'), ('bottle-formula', 'bottle-formula'), ('soft-food', 'soft-food')], max_length=25)),
                ('amount', models.IntegerField(help_text='in mL')),
                ('length_of_feeding', models.IntegerField(blank=True, help_text='minutes', null=True)),
                ('entered_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
