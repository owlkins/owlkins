# Generated by Django 3.1.3 on 2020-12-01 23:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0003_poop'),
    ]

    operations = [
        migrations.AlterField(
            model_name='poop',
            name='blowout',
            field=models.BooleanField(default=False, help_text='Indicate if it was a blowout, default is False'),
        ),
        migrations.AlterField(
            model_name='poop',
            name='diaper_size',
            field=models.CharField(choices=[('2', '2'), ('3', '3'), ('4', '4')], max_length=10),
        ),
        migrations.AlterField(
            model_name='poop',
            name='h',
            field=models.IntegerField(blank=True, help_text='height of photo in pixels, often set automatically on upload', null=True),
        ),
        migrations.AlterField(
            model_name='poop',
            name='image',
            field=models.ImageField(blank=True, height_field='h', help_text='if the poop was surprisingly memorable, you can upload a photo!', null=True, upload_to='poops', width_field='w'),
        ),
        migrations.AlterField(
            model_name='poop',
            name='w',
            field=models.IntegerField(blank=True, help_text='width of photo in pixels, often set automatically on upload', null=True),
        ),
    ]
