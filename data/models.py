from django.db import models
from django.contrib.auth.models import User
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, Transpose

SLEEP_TYPES = [
    'nap',
    'overnight',
]

FEEDING_TYPES = [
    'breast',
    'bottle-breastmilk',
    'bottle-formula',
    'soft-food'
]

POOP_TYPES = [
    'small',
    'medium',
    'large',
    'extra-large',
]

DIAPER_SIZES = [
    '1',
    '2',
    '3',
    '4',
    '5',
]

ML_TO_OUNCES = 0.033814
MINS_TO_OUNCES = 2 / 10.  # 2.oz / 10 mins

class BaseData(models.Model):
    entered_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    start = models.DateTimeField()
    end = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    extra = models.CharField(max_length=1024, null=True, blank=True, help_text='any notes or other info to record')

    class Meta:
        abstract = True


class Sleep(BaseData):
    type = models.CharField(max_length=15, choices=[(each, each,) for each in SLEEP_TYPES])
    temperature = models.IntegerField(null=True, blank=True, help_text='°F')

    @property
    def length(self):
        value = self._length
        if not value:
            return '(derived from start minus end)'
        hours = value // 3600
        minutes = (value - hours * 3600)// 60
        out = ''
        if hours:
            out += '%d hr%s. ' % (hours, 's' if hours > 1 else '')
        if minutes:
            out += '%d min%s.' % (minutes, 's' if minutes > 1 else '')
        return out

    @property
    def _length(self):
        """ Time in seconds between `start` and `end`
              or null if one is not populated

        :return: int:seconds
        """
        if self.start and self.end:
            return (self.end - self.start).total_seconds()

    @property
    def _length_in_hours(self):
        """ Time in hours between `start` and `end`
              or null if one is not populated

        :return: int:hours
        """
        return (self._length or 0) / 3600


class Feeding(BaseData):
    type = models.CharField(max_length=25, choices=[(each, each,) for each in FEEDING_TYPES])
    amount = models.IntegerField(help_text='in mL (if bottle)', null=True, blank=True)
    length_of_feeding = models.IntegerField(help_text='minutes (if breast)', null=True, blank=True)

    @property
    def amount_in_ounces(self):
        value = self._amount_in_ounces
        return '%0.1f' % value if value else '(derived from amount field)'

    @property
    def _amount_in_ounces(self):
        value = None
        if self.amount:
            value = self.amount * ML_TO_OUNCES
        elif self.length_of_feeding:
            value = self.length_of_feeding * MINS_TO_OUNCES
        return value or 0.

class Poop(BaseData):
    type = models.CharField(max_length=15, choices=[(each, each,) for each in POOP_TYPES])
    diaper_size = models.CharField(max_length=10, choices=[(each, each,) for each in DIAPER_SIZES])
    blowout = models.BooleanField(default=False, help_text='Indicate if it was a blowout, default is False')
    image = models.ImageField(upload_to="poops", height_field='h', width_field='w', null=True, blank=True,
                              help_text='if the poop was surprisingly memorable, you can upload a photo!')
    image_thumbnail = ImageSpecField(source='image',
                                     processors=[
                                         Transpose(),
                                         ResizeToFill(298, 172),
                                     ],
                                     format='JPEG',
                                     options={'quality': 60})
    w = models.IntegerField(null=True, blank=True, help_text='width of photo in pixels, often set automatically on upload')
    h = models.IntegerField(null=True, blank=True, help_text='height of photo in pixels, often set automatically on upload')