from data.models import Feeding, Sleep, Poop, DIAPER_SIZES, POOP_TYPES, SLEEP_TYPES, FEEDING_TYPES
from django.utils import timezone
import datetime
from typing import Optional



def average_feedings(start: Optional[datetime.datetime] = None, end: Optional[datetime.datetime] = None):
    feedings = Feeding.objects.all().order_by('start')
    if start:
        feedings = feedings.filter(start__gte=start)
    if end:
        feedings = feedings.filter(end__lte=end)
    dates = [each.date() for each in feedings.datetimes('start', 'day') if each.date() > datetime.date(2020, 12, 1)]
    amount_per_day = {date: 0 for date in dates}
    amount_per_feeding = []
    for date in dates:
        _filter_date = datetime.datetime(date.year, date.month, date.day, tzinfo=timezone.get_current_timezone())
        for feeding in feedings.filter(start__gte=_filter_date, start__lte=_filter_date + datetime.timedelta(days=1)):
            amount_per_day[date] += feeding._amount_in_ounces