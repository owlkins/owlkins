from django import template
from django.template.loader import get_template
from django.template.base import Node

register = template.Library()

class ModalNode(Node):
    def __init__(self, nodelist, kind):
        self.nodelist = nodelist
        self.kind = kind

    def render(self, context):
        code_snippet_html = get_template("data/modals/base_modal.html")
        return code_snippet_html.render({
            'modal_body': self.nodelist.render(context),
            'kind': self.kind
        }, request=context['request'])

@register.tag('data_base_modal')
def code_snippet(parser, token):
    nodelist = parser.parse(('end_data_base_modal',))
    tokens = token.split_contents()
    parser.delete_first_token()
    return ModalNode(nodelist, kind=tokens[1])
