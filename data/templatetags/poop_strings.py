from django.template import Library

register = Library()

@register.filter
def multiply(string, times):
    return string * times

@register.filter
def index(iterable, item):
    return iterable.index(item) if item in iterable else -1

@register.filter
def index_base_1(iterable, item):
    return index(iterable, item) + 1
