from django.shortcuts import render
from data.models import Feeding, Sleep, Poop, DIAPER_SIZES, POOP_TYPES, SLEEP_TYPES, FEEDING_TYPES
from data.views import POOP_OFFSET, SLEEP_OFFSET, _calculate_poop_score
from django.utils import timezone
import datetime
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from sentry_sdk import capture_exception
from .views import _get_today, _get_now

TIME_BINS = []
for i in range(24):
    for j in [0, 30,]:
        TIME_BINS.append(datetime.time(i, j))

COLORS = [
    'purple',
    'green',
    'blue',
    'orange',
    'yellow',
]
FEEDING_TYPE_COLORS = {
    FEEDING_TYPES[i]: COLORS[i] for i in range(len(FEEDING_TYPES))
}

SLEEP_TYPE_COLORS = {
    SLEEP_TYPES[i]: COLORS[i] for i in range(len(SLEEP_TYPES))
}

BIN_COLOR_TYPES = {
    'overnight': (156, 39, 176),
    'morning': (245, 148, 24),
    'afternoon': (24, 245, 42),
    'dinnertime': (24, 245, 236),
    'bedtime': (24, 30, 245),
}

BIN_COLORS = []

for t in TIME_BINS:
    if t < datetime.time(hour=6):
        color = 'overnight'
    elif t < datetime.time(hour=12):
        color = 'morning'
    elif t < datetime.time(hour=16):
        color = 'afternoon'
    elif t < datetime.time(hour=18):
        color = 'dinnertime'
    elif t < datetime.time(hour=20):
        color = 'bedtime'
    else:
        color = 'overnight'
    BIN_COLORS.append(BIN_COLOR_TYPES[color])

def _get_filter_date(date):
    return timezone.now().astimezone(timezone.get_current_timezone()).replace(
            year=date.year, month=date.month, day=date.day,
            hour=0, minute=0, second=0, microsecond=0)

@login_required
def feeding_trending(request):
    feedings = Feeding.objects.all().order_by('start')
    if not feedings:
        return render(request, 'data/trending/no_trending.html', {'trending_type': 'feedings'})
    start_date, end_date, dates, sleeps = _get_start_end_dates(request, feedings)
    moving_avg_days = int(request.GET.get('moving_avg_days', 5)) - 1
    amount_per_day = {each: {date: 0 for date in dates} for each in FEEDING_TYPES}
    feeding_time_binned = {each: 0 for each in TIME_BINS}
    feeding_types_per_day = {each: {date: 0 for date in dates} for each in FEEDING_TYPES}
    moving_average_per_day = []
    total_per_day = []
    previous_bin_type = None
    for date in dates:
        total_for_day = 0.
        _filter_date = _get_filter_date(date)
        for feeding in feedings.filter(
                start__gte=_filter_date, start__lte=_filter_date + datetime.timedelta(days=1)).order_by('start'):
            amount = feeding._amount_in_ounces
            # Feeding amount may be negative, in cases of adverse food reaction (texture, otherwise)
            # In that case, we should subtract from the previous meal.
            if previous_bin_type is None or amount > 0:
                amount_per_day[feeding.type][date] += amount
            else:
                amount_per_day[previous_bin_type][date] += amount
            total_for_day += amount
            previous_bin_type = feeding.type
            feeding_types_per_day[feeding.type][date] += 1
            for i in range(len(TIME_BINS)):
                if feeding.start.astimezone(timezone.get_current_timezone()).time() < TIME_BINS[i]:
                    feeding_time_binned[TIME_BINS[i-1]] += 1
                    break
        if not total_for_day and not total_per_day:
            continue
        elif not date == _get_now().date() and (total_for_day or request.GET.get('decaying_average')):
            moving_average_per_day.append(round((total_for_day + sum(total_per_day[-moving_avg_days:])) /
                                           (len(total_per_day[-moving_avg_days:]) + 1), 1))
        else:
            moving_average_per_day.append(moving_average_per_day[-1])
        total_per_day.append(total_for_day)
    content = {
        'amount_chart_labels': [str(date.date()) for date in dates],
        'amount_per_day': [(each, FEEDING_TYPE_COLORS[each], [round(amount_per_day[each][every], 1) for every in amount_per_day[each]],) for each in FEEDING_TYPES],
        'feeding_time_binned': [feeding_time_binned[each] for each in TIME_BINS],
        'feeding_time_binned_labels': [str(each) for each in TIME_BINS],
        'feeding_types_per_day': [(each, FEEDING_TYPE_COLORS[each], [feeding_types_per_day[each][every] for every in feeding_types_per_day[each]],) for each in FEEDING_TYPES],
        'FEEDING_BIN_COLORS': BIN_COLORS,
        'FEEDING_BIN_COLOR_TYPES': BIN_COLOR_TYPES,
        'moving_average_per_day': moving_average_per_day,
        'MOVING_AVG_DAYS': moving_avg_days + 1,
        'start_date': start_date,
        'end_date': end_date,
    }
    return render(request, 'data/trending/feeding_trending.html', content)


def _get_start_end_dates(request, model_query, offset=datetime.timedelta()):
    tz = timezone.get_current_timezone()
    get_date = lambda x: datetime.datetime.strptime(request.GET.get(x), '%a %b %d %Y').astimezone(tz)
    if request.GET.get('end'):
        end_date = get_date('end').replace(hour=23, minute=59, second=59) - offset
    else:
        end_date = model_query.last().start.astimezone(tz)
    if request.GET.get('start'):
        start_date = get_date('start') + offset
    else:
        start_date = end_date.replace(hour=0, minute=0, second=0) \
                    + offset \
                    - datetime.timedelta(days=int(request.GET.get('days_back', '30')))
    model_query = model_query.filter(start__gte=start_date, start__lte=end_date)
    dates = list({(each.start) for each in model_query})
    dates.sort()
    if not request.GET.get('no_blank_days'):
        start = dates[0] if len(dates) > 1 else start_date
        end = dates[-1] if len(dates) > 2 else end_date
        dates = [start]
        while dates[-1] < end:
            dates.append(dates[-1] + datetime.timedelta(days=1))
    return start_date, end_date - offset, dates, model_query

@login_required
def sleep_trending(request):
    sleeps = Sleep.objects.filter(end__isnull=False).order_by('start')
    if not sleeps:
        return render(request, 'data/trending/no_trending.html', {'trending_type': 'sleep'})
    start_date, end_date, dates, sleeps = _get_start_end_dates(request, sleeps, SLEEP_OFFSET)
    moving_avg_days = int(request.GET.get('moving_avg_days', 5)) - 1
    amount_per_day = {date: 0 for date in dates}
    sleep_time_binned = {each: 0 for each in TIME_BINS}
    sleep_types_per_day = {each: {date: 0 for date in dates} for each in SLEEP_TYPES}
    moving_average_per_day = []
    total_per_day = []
    for date in dates:
        total_for_day = 0.
        _filter_date = _get_filter_date(date) + SLEEP_OFFSET
        for sleep in sleeps.filter(start__gte=_filter_date, start__lte=_filter_date + datetime.timedelta(days=1)):
            amount = sleep._length_in_hours
            total_for_day += amount
            amount_per_day[date] += amount
            sleep_types_per_day[sleep.type][date] += amount
            for i in range(len(TIME_BINS)):
                if sleep.start.astimezone(timezone.get_current_timezone()).time() < TIME_BINS[i]:
                    sleep_time_binned[TIME_BINS[i-1]] += 1
                    break
        if not total_for_day and not total_per_day:
            continue
        elif not date == _get_now().date() and (total_for_day or request.GET.get('decaying_average')):
            moving_average_per_day.append(round((total_for_day + sum(total_per_day[-moving_avg_days:])) /
                                           (len(total_per_day[-moving_avg_days:]) + 1), 1))
        else:
            moving_average_per_day.append(moving_average_per_day[-1])
        total_per_day.append(total_for_day)
    content = {
        'amount_chart_labels': [str(date.date()) for date in dates],
        'amount_per_day': [round(amount_per_day[date], 1) for date in dates],
        'sleep_time_binned': [sleep_time_binned[each] for each in TIME_BINS],
        'sleep_time_binned_labels': [str(each) for each in TIME_BINS],
        'sleep_types_per_day': [(
            each,
            SLEEP_TYPE_COLORS[each],
            [round(sleep_types_per_day[each][every], 1) for every in sleep_types_per_day[each]],
        ) for each in ['overnight', 'nap']],
        'SLEEP_BIN_COLORS': BIN_COLORS,
        'SLEEP_BIN_COLOR_TYPES': BIN_COLOR_TYPES,
        'MOVING_AVG_DAYS': moving_avg_days + 1,
        'moving_average_per_day': moving_average_per_day,
        'start_date': start_date,
        'end_date': end_date,
    }
    return render(request, 'data/trending/sleep_trending.html', content)

@login_required
def poop_trending(request):
    poops = Poop.objects.all().order_by('start')
    if not poops:
        return render(request, 'data/trending/no_trending.html', {'trending_type': 'poops'})
    start_date, end_date, dates, poops = _get_start_end_dates(request, poops, POOP_OFFSET)
    amount_per_day = {date: [] for date in dates}
    poop_time_binned = {each: 0 for each in TIME_BINS}
    poop_types_per_day = {each: {date: [] for date in dates} for each in POOP_TYPES}
    for date in dates:
        _filter_date = _get_filter_date(date) + POOP_OFFSET
        for poop in poops.filter(start__gte=_filter_date, start__lte=_filter_date + datetime.timedelta(days=1)):
            amount_per_day[date].append(poop)
            poop_types_per_day[poop.type][date].append(poop)
            for i in range(len(TIME_BINS)):
                if poop.start.astimezone(timezone.get_current_timezone()).time() < TIME_BINS[i]:
                    poop_time_binned[TIME_BINS[i-1]] += 1
                    break
    content = {
        'amount_chart_labels': [str(date.date()) for date in dates],
        'amount_per_day': [_calculate_poop_score(amount_per_day[date]) for date in dates],
        'amount_per_day_colors': ['red' if any(each.blowout for each in amount_per_day[date]) else 'grey' for date in dates],
        'poop_time_binned': [poop_time_binned[each] for each in TIME_BINS],
        'poop_time_binned_labels': [str(each) for each in TIME_BINS],
        # 'poop_types_per_day': [(
        #     each,
        #     POOP_TYPE_COLORS[each],
        #     [poop_types_per_day[each][every] for every in poop_types_per_day[each]],
        # ) for each in POOP_TYPES],
        'POOP_BIN_COLORS': BIN_COLORS,
        'POOP_BIN_COLOR_TYPES': BIN_COLOR_TYPES,
        'start_date': start_date,
        'end_date': end_date,
    }
    return render(request, 'data/trending/poop_trending.html', content)
