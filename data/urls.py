from django.conf.urls import url

from . import views, trending

urlpatterns = [
    url(r'^$', views.dashboard, name='data_dashboard'),
    url(r'save', views.update_data, name='data_update_data'),
    url(r'trending/feeding', trending.feeding_trending, name='trending_feeding'),
    url(r'trending/sleep', trending.sleep_trending, name='trending_sleep'),
    url(r'trending/poop', trending.poop_trending, name='trending_poop'),
]
