from django.shortcuts import render
from data.models import Feeding, Sleep, Poop, DIAPER_SIZES, POOP_TYPES, SLEEP_TYPES, FEEDING_TYPES, ML_TO_OUNCES
from django.utils import timezone
import datetime
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from sentry_sdk import capture_message
from django.http.response import JsonResponse
from owlkins.config import LOCAL_CONFIG
import pytz


def _get_now():
    return timezone.now().astimezone(timezone.get_current_timezone())


def _get_today():
    return _get_now().replace(hour=0, minute=0, second=0, microsecond=0)


SLEEP_OFFSET = datetime.timedelta(hours=-6)
POOP_OFFSET = datetime.timedelta(hours=6.5)

@login_required
def dashboard(request):
    date = request.GET.get('date')
    if date:
        date = datetime.datetime.strptime(date, '%a %b %d %Y').date()
    today = _get_today()
    if date and date != today:
        today = today.replace(year=date.year, month=date.month, day=date.day)
    elif date == today.date():
        return HttpResponseRedirect('/data')
    feedings = Feeding.objects.filter(start__gte=today).order_by('start')
    sleep_cutoff = today + SLEEP_OFFSET
    sleeps = Sleep.objects.filter(start__gte=sleep_cutoff).order_by('start')
    poop_cutoff = today + POOP_OFFSET
    poops = Poop.objects.filter(start__gte=poop_cutoff).order_by('start')
    last_poop = None
    last_poop_tz = None
    if date:
        feedings = feedings.filter(start__lte=today + datetime.timedelta(days=1))
        sleeps = sleeps.filter(start__lte=sleep_cutoff + datetime.timedelta(days=1))
        poops = poops.filter(start__lte=poop_cutoff + datetime.timedelta(days=1))
    elif not poops:
        last_poop = Poop.objects.all().order_by('start').last()
        if last_poop and last_poop.start:
            last_poop_tz = last_poop.start.timestamp()
            last_poop = (timezone.now() - last_poop.start).total_seconds() / 3600
    last_update = max(each.order_by('updated_at').last().updated_at
                      if each else today for each in [feedings, sleeps, poops]).timestamp()
    if request.GET.get('check_last_update'):
        return JsonResponse({'last_update': last_update})
    content = {
        'date': date,
        'total_ounces': sum(each._amount_in_ounces for each in feedings),
        'feedings': feedings,
        'total_sleep': sum((each.end - each.start).total_seconds() / 3600. for each in sleeps if each.start and each.end),
        'sleeps': sleeps,
        'total_poop_score': _calculate_poop_score(poops),
        'poops': poops,
        'POOP_TYPES': POOP_TYPES,
        'FEEDING_TYPES': FEEDING_TYPES,
        'DIAPER_SIZES': DIAPER_SIZES,
        'SLEEP_TYPES': SLEEP_TYPES,
        'default_feeding_type': LOCAL_CONFIG.get('DEFAULT_FEEDING_TYPE') or 'breast',
        'default_feeding_amount': LOCAL_CONFIG.get('DEFAULT_FEEDING_AMOUNT') or 0,
        'sleep_cutoff': sleep_cutoff,
        'poop_cutoff': poop_cutoff,
        'last_poop': last_poop,
        'last_poop_timestamp': last_poop_tz,
        'last_update': last_update,
        'CURRENT_TIMEZONE': _get_now().strftime('%Z'),
        'DEFAULT_DIAPER_SIZE': str(LOCAL_CONFIG.get('DEFAULT_DIAPER_SIZE') or 1),
    }
    return render(request, 'data/dashboard.html', content)

def _calculate_poop_score(poops):
    total = 0.
    for poop in poops:
        if poop.type == 'small':
            total += 1
        elif poop.type == 'medium':
            total += 2
        elif poop.type == 'large':
            total += 3
        elif poop.type == 'extra-large':
            total += 5
    return total / 5. * 100


@login_required
def update_data(request):
    date = request.GET.get('date')
    if date:
        date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
    today = _get_today().date()
    if date and date != today:
        today = date
    response = HttpResponseRedirect('/data' + ('?date=' + date.strftime('%a %b %d %Y') if date else ''))
    if request.method == 'POST':
        typename = request.POST.get('typename')
        timezone_offset = request.POST.get('timezone_offset')
        if timezone_offset:
            timezone_offset = pytz.FixedOffset(-int(timezone_offset))
        if typename == 'poop':
            obj = Poop
        elif typename == 'feeding':
            obj = Feeding
        elif typename == 'sleep':
            obj = Sleep
        else:
            message = 'Invalid type {type}'.format(type=typename)
            messages.error(request, message)
            capture_message(message)
            return response
        if request.POST.get('id'):
            data = obj.objects.get(id=request.POST.get('id'))
            if request.POST.get('delete') == 'confirmed':
                data.delete()
                return response
        else:
            data = obj(entered_by=request.user)
        change_functions = {
            'blowout': lambda x: x == 'true'
        }
        for each in obj._meta.fields:
            name = each.name
            value = request.POST.get(name)
            if name in ['id', 'entered_by']:
                continue
            elif name == 'amount':
                value_in_oz = request.POST.get('amount_in_oz')
                if value_in_oz and request.POST.get('type') == 'soft-food':
                    value = float(value_in_oz) / ML_TO_OUNCES
            elif name in request.POST and (not value and getattr(data, name)):
                setattr(data, name, None)
                continue
            elif name in change_functions:
                value = change_functions[name](value)
            elif value and 'IntegerField' in str(each.__class__):
                value = int(float(value))
            elif value and 'DateTimeField' in str(each.__class__) and 'T' not in value:
                value_date = getattr(data, name)
                value_date = value_date.astimezone(timezone.get_current_timezone()).date() if value_date else today
                h, m, *_ = value.split(":")
                value = datetime.datetime(value_date.year, value_date.month, value_date.day, int(h), int(m))
                if hasattr(timezone_offset, 'localize'):
                    value = timezone_offset.localize(value)
                else:
                    value = timezone.get_current_timezone().localize(value)
            if value or name in change_functions:
                setattr(data, name, value)
        data.save()
    return response
