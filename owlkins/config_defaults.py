# This file is a configuration file that you can customize to bring your site to life with
# your child and related information such as your email.
#
# WARNING: This is NOT a file that you should store secrets in. That is handled via the file "secrets.py" or
# optionally a Hashicorp Vault secrets store.
#
# TO USE:  Copy this file within this directory and name it "config.py"

# Set this to True on the public facing machine serving as your production server
# In development, you can set this value to False and things like DEBUG will bet set to True
PROD = True

# LOCAL_CONFIG is a dictionary of variables that influence the look and feel of your Owlkins installation
# Some values are required, but if the value is optional, it can be commented out to disable it.
#
# We will try to give some sample values next to the parameter so that you can have an idea of the structure
# of the field
LOCAL_CONFIG = {

    # ----==== BASIC ITEMS ====----
    # The most important value once decided, your baby's name!
    'BABY_NAME': '',     # 'Joe'   Don't put a possessive apostrophe, we will add where appropriate
    # The domain that the server will host, feeds AUTHORIZED_HOSTS and also some email links
    'BASE_URL': '',      # 'baby.example.com'
    # The parents' names are displayed on outgoing email signatures as well as on the registry
    'PARENT_NAMES': '',  # 'Parent1 & Parent2',

    # ----==== EMAIL ====----
    # Your email, which will be the default "reply-to" for all outgoing emails.
    'ADMIN_EMAIL': '',                 #  'your.email@gmail.com'
    # The "From" display name sent alongside your email
    'DEFAULT_EMAIL_DISPLAY': '',       #  'Baby\'s Server', This can be possessive and will be who the email is "From"
    # Your SMTP host to send outgoing messages. Here defaulted for example to AWS SES in Oregon
    'EMAIL_HOST': '',                  # 'email-smtp.us-west-2.amazonaws.com'
    # Your authorized SMTP domain registered with your sending service, likely the same as "BASE_URL"
    'EMAIL_DOMAIN': '',                # 'example.com',    whichever domain you registered with the SMTP relay

    # ----==== S3 Compatible Storage ====----
    # If storing images in a S3 compatible system, provide the bucket name
    'AWS_STORAGE_BUCKET_NAME': '',     # 'your-s3-bucket'
    # If you are using AWS S3, or another service where the CDN is separate, provide the CDN here
    'AWS_CLOUDFRONT_DOMAIN': '',       # 'cdn.baby.example.com'
    # Your AWS IAM user authenticated to GET and PUT to the S3 bucket
    'ARN': '',                         # 'arn:aws:iam::XXXXXXXXX:user/your-baby-site-user'

    # ----==== Video conversion ====----
    # The directory where a video is temporarily written to disk, and ffmpeg called, where the converted video is also
    # stored. If the directory provided below begins with "/" it'll be treated as absolute path, else it'll be relative
    # to the Owlkins project root.
    'VIDEO_SCRATCH_DIR': '.video_scratch',
    # Flag to tell the web API if a systemd daemon has been set up to run the `convert_all_pending_videos` Django
    # management command periodically. This command should be set up to periodically run the following from within
    # the virtual environment to convert all pending videos.
    #   >>> python manage.py convert_all_pending_videos
    # If not set up, Owlkins will attempt to spawn a thread
    # per video and spawn the ffmpeg conversion command in a non-conflicting way, though the threading approach
    # does sometimes drop videos if too many are uploaded at once.
    'VIDEO_DAEMON_SET_UP': False,

    # Provide your time zone. You can list available timezones with the following commands in a Python console:
    # >>> import pytz
    # >>> pytz.all_timezones
    'TIME_ZONE': 'America/Chicago',

    # ----==== REGISTRY ONLY SETTINGS ====----
    # The parents' address is displayed on the registry
    'PARENT_ADDRESS1': '',      # '123 Fake St.'
    'PARENT_ADDRESS2': '',      # 'Big City, NS XXXXX'
    # The parent's email who is organizing the registry (usually the mom). May be different or the same as the ADMIN
    'PARENT_EMAIL': '',         # 'parent@example.com'

    # ----==== ROUTING ====----
    # The following options allow you to change which pages are displayed to the user, and also which page is
    # shown when a user visits your domain's root address.
    #
    # HIDE_PHOTOS disables the Photos Feature for all users in the Navbar
    'HIDE_PHOTOS': False,
    # HIDE_REGISTRY disables the Registry Feature for all users in the Navbar
    'HIDE_REGISTRY': False,
    # HIDE_DATA disables the Data Feature for all users in the Navbar
    'HIDE_DATA': False,
    # DEFAULT_PAGE is the Feature that will be shown on the root domain URL
    # Valid values are:
    #    - Photos           [default]
    #    - Registry
    #    - Data
    'DEFAULT_PAGE': 'Photos',

    # ----==== MISC ====----
    # Show in emails and other places information about this open source project. True if enabled
    'SHOW_ATTRIBUTION': True,
    # Optional image to display on the jumbotron at the bottom of the page
    'BOTTOM_IMAGE': 'https://static.owlkins.com/v1/images/orange_owl.png',
    # Optional image to display in the header jumbotron on the registry
    'TOP_IMAGE': 'https://static.owlkins.com/v1/images/blue_pink_owl_export.png',

    # The feeding form on the /data page has a drop down for the type of feeding. The choices are
    #   * breast
    #   * bottle-breastmilk
    #   * bottle-formula
    #   * soft-food
    # Select one of these and put in the DEFAULT_FEEDING_TYPE. If desired, a prepopulated amount can be given in
    # DEFAULT_FEEDING_AMOUNT, for example if you routinely fill a formula bottle to a certain amount.
    'DEFAULT_FEEDING_TYPE': 'bottle-formula',
    'DEFAULT_FEEDING_AMOUNT': None,

    # The Poop form has a drop-down for the diaper size used for the poop (if for example the parent is tracking
    # what size does or does not cause a "blowout").  Enter any integer in this field (1 through 5) for the "default"
    # value in the drop-down (so that you ideally don't have to change it all the time).
    'DEFAULT_DIAPER_SIZE': 0,      # 0 or omitting will default to 1


    # ====---- Sending Errors to Owlkins' Sentry ----====
    # Owlkins uses a self hosted Sentry server to track errors for the purpose of making the site better.
    # To opt-in, set "SEND_ERRORS_TO_SENTRY" to True. If SEND_ERRORS_TO_SENTRY is not True, the other settings are not
    # evaluated
    #
    # To send the Sentry errors to your own Sentry server, set variable "SENTRY_DSN" to the endpoint in your KEYS
    # file.
    'SEND_ERRORS_TO_SENTRY': False,
    # If you wish to associate users to errors (assuming you are using django.contrib.auth) you may
    # enable sending PII data.
    'SEND_PII_TO_SENTRY': False,
    # Your custom "backend" tracker for Python errors
    'SENTRY_DSN': '',
    # Your custom "frontend" tracker for Javascript errors
    'SENTRY_FRONTEND_DSN': '',

    # ====---- Using Hashicorp Vault instead of secrets.py ----====
    # The default installation steps instruct you to make a "secrets.py" file based on "secrets_defaults.py"
    #
    # The Vault secrets store has the passwords and other sensitive information stored. You should have separate
    # secrets stores for each environment; configure them here. If VAULT_ENVS or VAULT_KV_STORE are missing, the
    # secrets store will attempt to load all secrets from environment variables instead, so comment these out if you plan
    # to use environment variables only
    #
    #
    'VAULT_SERVER': '',             # https://yourvaultserver.dev
    # VAULT_KV_STORE is the name of the key-value store on your vault server
    'VAULT_KV_STORE': '',          #  'the_name_of_your_kv_store',
    # VAULT_ENVS a list of secrets that will be checked sequentially and loaded. Values from earlier secrets can be
    # overridden if you have more than one secret to read (such as perhaps DEV and UAT combined for some reason), where
    # subsequent secrets will overwrite values in the overall dictionary.
    'VAULT_ENVS': [],              #  ['your_secret_store_name']   note that this is a list of strings
}
