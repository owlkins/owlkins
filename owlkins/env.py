import os
import sys
import logging
try:
    from .config import PROD
    assert isinstance(PROD, bool), 'PROD variable must be Boolean (True or False)'
except ModuleNotFoundError:
    raise ModuleNotFoundError(
        "No config.py found\n\nYou must have a valid `config.py` file at `{project_root}/owlkins/config.py`\n\n"
          "See `{project_root}/owlkins/config_defaults.py` for an example structure for config.py\n\n"
    )
except (Exception, AssertionError) as e:
    logging.warning('Setting PROD to True (Debug to False): {}'.format(e), exc_info=e)
    PROD = True

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# The TESTING flag to determine if we are in unit test mode.
TESTING = sys.argv[1:2] == ['test']
