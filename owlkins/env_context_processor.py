from django.conf import settings

def env_context_processor(request):
    content = {
        'BABY_NAME': settings.LOCAL_CONFIG['BABY_NAME'],
        'BOTTOM_IMAGE': settings.LOCAL_CONFIG.get('BOTTOM_IMAGE', 'https://static.owlkins.com/v1/images/orange_owl.png'),
        'TOP_IMAGE': settings.LOCAL_CONFIG.get('TOP_IMAGE', 'https://static.owlkins.com/v1/images/blue_pink_owl_export.png'),
        'HIDE_PHOTOS': settings.LOCAL_CONFIG.get('HIDE_PHOTOS'),
        'HIDE_REGISTRY': settings.LOCAL_CONFIG.get('HIDE_REGISTRY'),
        'HIDE_DATA': settings.LOCAL_CONFIG.get('HIDE_DATA'),
        'DEFAULT_PAGE': settings.LOCAL_CONFIG.get('DEFAULT_PAGE'),
        'PROD': settings.PROD,
        'iOS': is_iOS(request)
    }
    if settings.LOCAL_CONFIG.get('SEND_ERRORS_TO_SENTRY'):
        content['SENTRY_FRONTEND_DSN'] = settings.LOCAL_CONFIG.get('SENTRY_FRONTEND_DSN')
        content['SENTRY_BACKEND_DSN'] = settings.LOCAL_CONFIG.get('SENTRY_DSN')
    return content

def is_iOS(request):
    return any(each in request.META.get('HTTP_USER_AGENT','') for each in ['iPhone', 'iPad']) if request and request.META else None

