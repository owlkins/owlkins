# This file is the file that you should set variables in that are not meant to be shared.
# Do not commit this file to version control or expose it publicly in any way! If you accidentally expose this file,
# you should change the values in all upstream systems.
#
# WARNING: This is NOT a file that you should store secrets in. That is handled via the Vault secrets store.
#
# TO USE:  Copy this file within this directory and name it "secrets.py"

KEYS = {
    # Django needs a SECRET_KEY defined in settings.py.   Rather than storing it in that file, set it here
    # and it will be loaded within settings.py. To generate a new key, see https://humberto.io/blog/tldr-generate-django-secret-key/
    'SECRET_KEY': '',

    # Owlkins needs a Database to operate. Django supports many but PostgreSQL is the one most extensively used with
    # Owlkins. Put the database name, user, and password here.
    'DB_SERVER': '',
    'DB_NAME': '',
    'DB_USER': '',
    'DB_PASSWORD': '',

    # Set your SMTP username and password here.
    # Note that the SMTP server itself is configured in "config.py" along with other email display settings.
    'EMAIL_HOST_USER': '',
    'EMAIL_HOST_PASSWORD': '',

    # If you are using S3 and Cloudfront to host static content such as images and videos, which is recommended,
    # store your access credentials here
    'AWS_ACCESS_KEY_ID': '',
    'AWS_SECRET_ACCESS_KEY': '',
    'CLOUDFRONT_KEYPAIR_ID': '',
    # Paste entire key in string or if stored in a file, put logic to load from file here.
    'CLOUDFRONT_PRIVATE_KEY': '',
}
