"""owlkins URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import owlkins.views as views

DEFAULT_PAGE = settings.LOCAL_CONFIG.get('DEFAULT_PAGE')

urlpatterns = [
    url(r'^{}'.format('' if DEFAULT_PAGE == 'Photos' else 'photos/'), include('photos.urls'), name='photo_base'),
    url(r'^{}'.format('' if DEFAULT_PAGE == 'Registry' else 'registry/'), include('registry.urls'), name='registry_base'),
    url(r'^users/', include('users.urls'), name='users_base'),
    url(r'^{}'.format('' if DEFAULT_PAGE == 'Data' else 'data/'), include('data.urls'), name='data_base'),
    path('admin/', admin.site.urls),
    # ETC miscellantions administration links
    url(r'^do-lb-check-alive', views.checkalive, name='checkalive'),
    url(r'^test-error-backend', views.test_error_backend, name='test_error_backend'),
    url(r'^test-error-frontend', views.test_error_frontend, name='test_error_frontend'),
]
if DEFAULT_PAGE == 'Data':
    urlpatterns.append(url(r'^data/', include('data.urls'), name='data_canonical'))

if settings.DEBUG is True:
    # import debug_toolbar
    urlpatterns += staticfiles_urlpatterns()
    # urlpatterns.append(path('__debug__/', include(debug_toolbar.urls)))


handler404 = views.notfound
handler500 = views.error