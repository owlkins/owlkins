import requests
import json
import logging
from .env import *
from .config import LOCAL_CONFIG

try:
    from .secrets import * # noqa
except ModuleNotFoundError:
    VAULT_ENVS = LOCAL_CONFIG.get('VAULT_ENVS')
    assert VAULT_ENVS, 'No ENVS configured'

    VAULT_KV_STORE = LOCAL_CONFIG.get('VAULT_KV_STORE')
    assert VAULT_KV_STORE, 'No KV Store'

    username = os.getenv('VAULT_USER')
    password = os.getenv('VAULT_PASSWORD')
    assert username and password, "No VAULT_USER and VAULT_PASSWORD in environment variables."

    SERVER = LOCAL_CONFIG.get('VAULT_SERVER')
    assert SERVER, 'No VAULT_SERVER in config.py'
    LOGIN_URL = SERVER.strip("/") + '/v1/auth/userpass/login/'
    logging.info('Attempting login with user: %s' % username)
    login = requests.post(LOGIN_URL+username, data=json.dumps({'password': password}))
    assert login.status_code == 200, "Unsuccessful login: %s" % login

    logging.info("Initializing vault key value store data acquisition at server %s" % SERVER)
    KEYS = {}
    for env in VAULT_ENVS:
        logging.info('Checking ENV %s" % env')
        DATA_URL = SERVER + '/v1/{kv_store}/data/{env}'.format(kv_store=VAULT_KV_STORE, env=env)
        logging.info("Login successful, getting data for environment %s" % env)
        token = json.loads(login.content)['auth']['client_token']
        response = requests.get(DATA_URL, headers={'X-Vault-Token': token})
        assert response.status_code == 200, "Unsuccessful data request: %s for env %s" % (response, env,)
        KEYS.update(json.loads(response.content)['data']['data'])
        logging.info("Successfully loaded %d keys from the vault" % len(KEYS))

