from django.shortcuts import render
from django.http import JsonResponse
from django.conf import settings
import logging
from sentry_sdk import capture_exception

logger = logging.getLogger(__name__)

def notfound(request, exception):
    if not settings.TESTING: logger.warning('404 not found at ' + request.path)
    return render(request, 'not_found.html', {}, status=404)

def error(request):
    content = {}
    if not settings.TESTING: capture_exception()
    return render(request, 'error.html', content, status=500)

def checkalive(request):
    return JsonResponse({'status':'ok'})

def test_error_backend(request):
    """ This function will always raise a ValueError"""
    raise ValueError("This is a test error")

def test_error_frontend(request):
    """ This function will server a page with Javascript errors"""
    return render(request, 'test_error_frontend.html', {}, status=200)