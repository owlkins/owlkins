from django.contrib import admin
from .models import Photo


class PhotoAdmin(admin.ModelAdmin):
    list_display = [each.name for each in Photo._meta.fields]

admin.site.register(Photo, PhotoAdmin)