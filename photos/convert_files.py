import multiprocessing
import logging
import urllib.request
import os
from photos.templatetags.signed_urls import sign_url
from django.core.files import File
import django.db
import time
from django.core.cache import cache
from sentry_sdk import capture_exception
from sentry_sdk.integrations.serverless import serverless_function
import cv2
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.files.base import ContentFile
from PIL import Image, ImageDraw, ImageFont
from shlex import quote
import datetime
from django.conf import settings
from pathlib import Path
import pickle


def replace_filename(fname):
    return str(fname).rsplit(".", 1)[0] + '_converted.mp4'

FFMPEG_COMMAND = 'ffmpeg -y -loglevel quiet -i {filename_in} -strict -2 -f mp4 ' \
                 '-threads {nprocs} -movflags +faststart -filter:v fps=30 ' \
                 '{filename_out}'

cache_key = 'Processing-Video'

@serverless_function
def _error_wrapper(func):
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as e:
            # Use synchronous exception handling otherwise our process will exit before we transmit
            capture_exception()
            logging.exception(e)
            cache.delete(cache_key)
    return wrapper


DEFAULT_FFMPEG_CPU_USAGE = os.cpu_count() - 1 or 1

class Convert2Mp4(multiprocessing.Process):
    """ Spawn a separate process to complete the processing of MOV -> MP4
    """

    def __init__(self, video, filename=None, file=None, nprocs=None, timezone_offset=None):
        self.video = video
        self.filename = filename
        self.file = file
        self.nprocs = nprocs or DEFAULT_FFMPEG_CPU_USAGE
        self.timezone_offset = timezone_offset
        multiprocessing.Process.__init__(self)

    @_error_wrapper
    def run(self):
        django.db.connection.close()
        logging.info('Converting mp4 for video {video}'.format(video=self.video))
        while cache.get(cache_key):
            logging.info('Already processing due to {cache_key}={value}'.format(cache_key=cache_key, value=cache.get(cache_key)))
            time.sleep(2)
        cache.set(cache_key, True)
        convert_file(self.video, self.filename, self.file, self.nprocs, self.timezone_offset)


def try_get_video_creation_time(video, filename_in, timezone_offset):
    FLAG_ORDER = [
        'CreationDate',
        'MediaCreateDate',
        'CreateDate',
        'TrackCreateDate',
    ]
    for flag in FLAG_ORDER:
        command = 'exiftool -{flag} {filename_in}'.format(flag=flag, filename_in=filename_in)
        output = os.popen(command).read()
        if output:
            creation_string = output.split(":", 1)[-1].strip()
            try:
                try:
                    # Format like: 2021:10:31 13:34:12
                    creation_time = datetime.datetime.strptime(creation_string, '%Y:%m:%d %H:%M:%S')
                except ValueError:
                    # Format like: 2021:11:05 23:15:48-05:00
                    creation_time = datetime.datetime.strptime(''.join(creation_string.rsplit(":", 1)),
                                                          '%Y:%m:%d %H:%M:%S%z')
            except Exception as e:
                logging.exception(e)
                capture_exception()
                continue
            if creation_time.tzinfo is None and hasattr(timezone_offset, 'localize'):
                creation_time = timezone_offset.localize(creation_time)
            # For cases where creation time is just from the ffmpeg command and in "server time"
            # Example: A user in Chicago uploads a video that should be -0600, so we stamp the creation_time
            #          as -0600, but "creation_time" is actually from the newly created file on and the server
            #          is running UTC, we then end up with a "creation_time" 6 hrs in the future. In fact
            #          this leads to the case where no creation time should be greater than the time it was
            #          uploaded. In this scenario, ignore.
            if creation_time < video.uploaded_time:
                video.created_time = creation_time
            video.save()
            return


def make_video_thumbnail(video, filename_out):
    success, image = cv2.VideoCapture(filename_out).read()
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = Image.fromarray(image)
    draw = ImageDraw.Draw(image)
    w, h = image.size
    smaller = min(w, h)
    fnt = ImageFont.truetype("photos/fonts/fontawesome-regular.ttf", int(smaller / 2))
    draw.text((w / (3.5 if w == smaller else 3), h / (3.5 if h == smaller else 3)),
              "\uf144", font=fnt,
              fill=(255, 255, 255))

    buffer = BytesIO()
    image.save(fp=buffer, format='JPEG')
    image_name = Path(Path(filename_out).stem, '.jpg')
    image_file = InMemoryUploadedFile(
        file=ContentFile(buffer.getvalue()),
        name=image_name,
        content_type='image/jpeg',
        field_name=None, size=image.tell, charset=None,
    )
    video.image.save(image_name, File(image_file))


def convert_file(video, filename=None, file=None, nprocs=DEFAULT_FFMPEG_CPU_USAGE, timezone_offset=None):
    scratch_dir = Path(settings.VIDEO_SCRATCH_DIR, str(video.id))
    scratch_dir.mkdir(parents=True, exist_ok=True)
    with open(Path(scratch_dir, 'meta'), 'rb') as f:
        meta = pickle.load(f)
    if filename is None:
        filename = meta.get('filename')
    timezone_offset = timezone_offset or meta.get('timezone_offset')
    orig_name = filename or video.video.name
    filename_in = Path(scratch_dir, orig_name)
    filename_out = replace_filename(filename_in)
    if file:
        with open(filename_in, 'wb') as f:
            f.write(file.read())
    elif not filename_in.exists():
        urllib.request.urlretrieve(sign_url(video.video.url, check_prod=False), filename_in)
    # shlex.quote -> https://stackoverflow.com/a/54099232/4765536
    command = FFMPEG_COMMAND.format(filename_in=quote(str(filename_in)),
                                    filename_out=quote(filename_out),
                                    nprocs=nprocs)
    logfile_name = '{}_log.txt'.format(filename_out)
    command = "FFREPORT=file={logfile}:level=32 ".format(logfile=quote(logfile_name)) + command
    return_value = os.system(command)
    assert return_value == 0, 'Video convert command failed for %s' % filename_in
    with open(filename_out, 'rb') as f:
        name = replace_filename(filename or video.video.name.split("/")[-1])
        video.video.save(name, File(f, name=name))
    try_get_video_creation_time(video, filename_in, timezone_offset)
    make_video_thumbnail(video, filename_out)
    if filename_in.suffix.upper() == '.MOV':
        if file:
            file.seek(0)
            f = file
        else:
            f = open(filename_in, 'rb')
        video.video_ios.save(orig_name, File(f, name=orig_name))
        if hasattr(f, 'close') and callable(f.close):
            f.close()
    os.remove(filename_in)
    os.remove(filename_out)
    # os.remove(logfile_name)
