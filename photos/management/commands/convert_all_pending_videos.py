from django.core.management.base import BaseCommand
import datetime
import logging
from photos.convert_files import convert_file
from django.conf import settings
from pathlib import Path
from photos.models import Photo
import pickle

logger = logging.getLogger('django')


class Command(BaseCommand):
    """ Run this command to find all non-deleted "pending processing" entries in the Photo table, and then process them.

    The unprocessed video data should be stored in a folder with the database ID within settings.VIDEO_SCRATCH_DIR

    We should expect that at most only one instance of this should run at a time as the ffmpeg command should be run
    such that it consumes the maximum amount of available processing per video.
    """
    help = "Attempt to run ffmpeg on any videos pending conversion"
    timeString = str(datetime.datetime.now()).replace(":", "-")   # When we started the job
    errors = []

    def add_arguments(self, parser):
        parser.add_argument('--failure_count',
                            dest='failure_count',
                            default=3,
                            help='Amount of times before we flag the video for deletion from the database.')

    def handle(self, *args, **options):
        videos_to_convert = Photo.objects.filter(image='videos/video_pending_processing.png', deleted=False)
        _ids_to_convert = ', '.join([str(video.id) for video in videos_to_convert])
        logger.info(f'There are currently {len(videos_to_convert)} to process:\n{_ids_to_convert}')
        for video in videos_to_convert:
            logging.info(f'Attempting to process {video}')
            try:
                convert_file(video)
            except Exception as e:
                logging.exception(e)
                meta_path = Path(settings.VIDEO_SCRATCH_DIR, str(video.id), 'meta')
                if meta_path.exists():
                    with open(meta_path, 'rb') as f:
                        meta = pickle.load(f)
                else:
                    meta = {}
                failure_count = meta.get('failure_count', 0)
                failure_count += 1
                logging.warning(f'Current failure count for {video} is {failure_count}.')
                if failure_count >= options['failure_count']:
                    video.deleted = True
                    video.save()
                else:
                    meta['failure_count'] = failure_count
                    with open(Path(settings.VIDEO_SCRATCH_DIR, str(video.id), 'meta'), 'wb') as f:
                        pickle.dump(meta, f)

        logging.info(f'Finished work on {len(videos_to_convert)} videos.')
