from django.core.management.base import BaseCommand
import datetime
import logging
import django.db
import time
from sentry_sdk import capture_exception
from django.contrib.auth.models import User
from users.email import BaseEmail
from photos.models import Photo
from photos.templatetags.signed_urls import sign_url
from users.models import Verification
from users.utils import generate_verification_key
from django.conf import settings
import pytz
from photos.templatetags.names import possessive


logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = "Email to be sent out daily"
    logFileDir = None           # Override in subclass
    logFileName = None          # Set in `handle`
    logger = logger             # Default logger
    printToScreen = False       # Print to screen flag for status messages (false for background job)
    sleepTime = 0.5             # The seconds to sleep between emails
    timeString =  str(datetime.datetime.now()).replace(":", "-")   # When we started the job
    errors = []

    def add_arguments(self,parser):
        parser.add_argument('--daemonJob',
                            action='store_true',
                            dest='daemonJob',
                            default=False,
                            help='Flag for the forever running job version.')

    def handle(self, *args, **kwargs):
        django.db.connection.close()  # Need to close DB connection so it re-opens after fork() if multiproccessed
        yesterday = datetime.datetime.now().replace(tzinfo=pytz.timezone(settings.TIME_ZONE)) - datetime.timedelta(days=1)
        if Verification.objects.filter(kind='skip_digest', key=str(yesterday.date())):
            self.log('Skipping emails for date {date}'.format(date=yesterday.date()))
            return
        self.send_all_emails(date=yesterday.replace(hour=0, minute=0, second=0, microsecond=0))

    def log(self, message, kind='info'):
        if self.printToScreen: print(message)
        if self.logFileName:
            with open(self.logFileName, 'a') as log_file:
                log_file.write(message + '\n')
        if kind == 'error':
            self.logger.error(message)
        elif kind == 'debug':
            self.logger.debug(message)
        else:
            self.logger.info(message)

    def send_all_emails(self, date):
        new_photos = Photo.objects.filter(
            uploaded_time__gte=date,
            uploaded_time__lte=date + datetime.timedelta(days=1),
            deleted=False
        )
        if len(new_photos) == 0:
            return
        users = User.objects.all().exclude(password__exact='')
        for user in users:
            if hasattr(user, 'emaildigest') and user.emaildigest.opted_out: continue
            self.log('Sending email to {user}'.format(user=user))
            photos_count = new_photos.filter(video__exact='').count()
            videos_count = new_photos.exclude(video__exact='').count()
            email = BaseEmail(
                subject='[{date}]{photo}{and_included}{video} for {baby_name}!'.format(
                    baby_name=settings.LOCAL_CONFIG['BABY_NAME'],
                    date=datetime.datetime.strftime(date, '%b %d'),
                    photo=' {n} new photo{s}'.format(n=photos_count, s='s' if photos_count > 1 else '') if photos_count else '',
                    and_included=' and' if photos_count and videos_count else '',
                    video=' {videos} video{s}'.format(videos=videos_count, s='s' if videos_count >1 else '')  if videos_count else '',
                ),
                user=user,
                body=self._get_new_photo_message(user, new_photos, date),
                greeting='Greetings',
                opt_out_message=self._get_opt_out_link(user),
            )
            email.sendMessage()
            time.sleep(self.sleepTime)


    def _get_new_photo_message(self, user, new_photos, date):
        site = "https://" + settings.BASE_URL
        message = 'For {date} there {is_are} {n} new item{s} ready to view on <a href="{site}">{baby_name_possessive} Photos</a>!'.format(
            baby_name_possessive=possessive(settings.LOCAL_CONFIG['BABY_NAME']),
            is_are='are' if len(new_photos) > 1 else 'is',
            s='s' if len(new_photos) > 1 else '',
            site=site,
            date=datetime.datetime.strftime(date, '%b %d'),
            n=len(new_photos),
        )
        if new_photos:
            message += '\n\nHere are a few samples:'
            message += '<table><tr>'
            for i in range(min(3, len(new_photos))):
                photo = new_photos[i]
                message += '<td style="padding:10px"><a href={site}><img src="{url}" style="border-radius: 15px 45px 15px 45px"></a></td>'.format(
                    site=site + '/#&gid=1&pid=' + str(photo.id),
                    url=sign_url(
                        photo.image_thumbnail.url,
                        expiration_seconds=50*24*60*60, # 50 days
                        check_prod=False),
                )
            message += "</tr></table>\n\n"
        return message

    def _get_opt_out_link(self, user):
        site = "https://" + settings.BASE_URL
        message = ''
        verification = Verification.objects.filter(
            kind='email_digest_opt_out',
            user=user,
            key_expires__gt=datetime.datetime.utcnow().replace(tzinfo=pytz.utc) + datetime.timedelta(days=20),
            key_used=None,
        )
        if verification:
            verification = verification[0]
        else:
            cancellation_key = generate_verification_key(user.username)
            verification = Verification(
                kind='email_digest_opt_out',
                user=user,
                key=cancellation_key,
                key_expires= datetime.datetime.utcnow().replace(tzinfo=pytz.utc) + datetime.timedelta(days=180),
            )
            verification.save()

        message += 'To opt out of this digest, please <a href="{opt_out}" style="color:white">click here</a>.'.format(
            opt_out=site + '/users/email_digest_opt_out?key=' + verification.key,
        )
        return message