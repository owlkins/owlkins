from django.core.management.base import BaseCommand
import datetime
import logging
from photos.models import Photo
import os
import requests
from photos.templatetags.signed_urls import sign_url
from photos.convert_files import make_video_thumbnail


logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = "Reprocess all video thumbnails"
    logFileDir = None           # Override in subclass
    logFileName = None          # Set in `handle`
    logger = logger             # Default logger
    printToScreen = False       # Print to screen flag for status messages (false for background job)
    timeString =  str(datetime.datetime.now()).replace(":", "-")   # When we started the job
    errors = []

    def add_arguments(self,parser):
        parser.add_argument('--daemonJob',
                            action='store_true',
                            dest='daemonJob',
                            default=False,
                            help='Flag for the forever running job version.')

    def handle(self, *args, **kwargs):
        videos = Photo.objects.filter(image__exact='').exclude(video__exact='')
        for video in videos:
            print('Working on %s' % video)
            video_url = sign_url(video.video.url, check_prod=False)
            filename_out = '/tmp/' + video_url.split("/")[-1].split("?")[0]
            with requests.get(video_url, stream=False) as r:
                with open(filename_out, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=1024 * 1024):
                        if chunk:
                            f.write(chunk)
            make_video_thumbnail(video, filename_out)
            os.remove(filename_out)
