from django.db import models
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, Transpose
from django.contrib.auth.models import User

class Photo(models.Model):
    image = models.ImageField(upload_to="photos", height_field='h', width_field='w', null=True, blank=True)
    image_thumbnail = ImageSpecField(source='image',
                                     processors=[
                                         Transpose(),
                                         ResizeToFill(298, 172),
                                     ],
                                     format='JPEG',
                                     options={'quality': 60})
    w = models.IntegerField(null=True, blank=True)
    h = models.IntegerField(null=True, blank=True)
    video = models.FileField(upload_to='videos', null=True, blank=True)
    video_ios = models.FileField(upload_to='videos_ios', null=True, blank=True)
    description = models.CharField(max_length=512, null=True, blank=True)
    deleted = models.BooleanField(default=False)
    created_time = models.DateTimeField(null=True, blank=True)
    uploaded_time = models.DateTimeField(null=True, blank=True)
    uploaded_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return '[%d]%s' % (self.id, self.image,)
