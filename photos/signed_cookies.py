from owlkins.vault import KEYS
import base64
import datetime
from django.conf import settings
# https://cryptography.io as an RSA backend
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.serialization import (
    load_pem_private_key
)
from botocore.signers import CloudFrontSigner
from django.utils import timezone
from django.contrib import messages

KEY = None
if KEYS.get('CLOUDFRONT_PRIVATE_KEY'):
    KEY = load_pem_private_key(KEYS['CLOUDFRONT_PRIVATE_KEY'].encode('ascii'), password=None, backend=default_backend())
    rsa_signer = lambda x: KEY.sign(x, padding.PKCS1v15(), hashes.SHA1())

COOKIE_LENGTH = 20  # number of days
COOKIES_TO_SET = [
    'CloudFront-Policy',
    'CloudFront-Signature',
    'CloudFront-Key-Pair-Id'
]

def sign_cookie(response):
    domain = settings.BASE_URL
    if not KEY:
        return response
    cloudfront_signer = CloudFrontSigner(KEY, rsa_signer=rsa_signer)
    expires = datetime.datetime.utcnow() + datetime.timedelta(days=COOKIE_LENGTH)
    # noinspection PyTypeChecker
    json_policy = cloudfront_signer.build_policy(
        resource="https://cdn.{domain}/*".format(domain=domain),
        date_less_than=expires,
    ).encode('utf-8')
    whitespace_replacements = [
        b'\n',
        b'\t',
        b' ',
    ]
    for value in whitespace_replacements:
        json_policy = json_policy.replace(value, b'')
    signature = rsa_signer(json_policy)
    signature = base64.b64encode(signature,)
    coded_policy = base64.b64encode(json_policy)
    replacements = [
        [b'/', b'~'],
        [b'+', b'-'],
        [b'=', b'_'],
    ]
    for value, target in replacements:
        signature = signature.replace(value, target)
        coded_policy = coded_policy.replace(value, target)
    kwargs = {
        'domain': domain,
        'secure': settings.PROD,
        'expires': expires,
        'samesite': 'None'
    }
    response.set_cookie('CloudFront-Policy', value=coded_policy.decode('utf-8'), **kwargs)
    response.set_cookie('CloudFront-Signature', value=signature.decode('utf-8'), **kwargs)
    response.set_cookie('CloudFront-Key-Pair-Id', value=KEYS['CLOUDFRONT_KEYPAIR_ID'], **kwargs)
    return response

def _cookie_version_change(last_cookie):
    """
    Return True if the cookie was set prior to some fundamental change in requirements for the cookie

    Return False if the cookie was set after the pivotal change.

    Increment the `logic_changed` date as necessary.
    :param last_cookie: datetime.datetime
    :return: bool
    """
    logic_changed = datetime.datetime(2020, 11, 25)
    return datetime.datetime.fromtimestamp(last_cookie) < logic_changed

def _cookie_approaching_expiriation(last_cookie, now):
    """
    Return True if the cookie is within half the time given before expiration, then we should renew it.
    :param last_cookie: datetime.datetime
    :param now: datetime.datetime
    :return: bool
    """
    return datetime.datetime.fromtimestamp(last_cookie).astimezone(timezone.get_current_timezone()) + \
           datetime.timedelta(days=int(COOKIE_LENGTH/2)) < now

def _cookie_missing_keys(request):
    """
    Return True if one of the cookie key:value pairs is missing as then we need to renew it.
    :param request:
    :return: bool
    """
    return all(request.COOKIES.get(each) for each in COOKIES_TO_SET)

def add_signed_cookie(function):
    def _add_signed_cookie(request):
        if not KEY and request.user.is_staff:
            messages.error(request, "Message for Admins: For Photos to work, secrets.py must contain a private key to "
                                    "sign cookies: CLOUDFRONT_PRIVATE_KEY")
        response = function(request)
        now = timezone.now()
        request.session['last_visit'] = now.timestamp()
        last_cookie = request.session.get('last_cookie_set')
        if not last_cookie or \
                _cookie_version_change(last_cookie) or \
                _cookie_approaching_expiriation(last_cookie, now) or \
                not _cookie_missing_keys(request):
            # If using Cloudfront signed cookies, we perodically need to recompute and send a new cookie
            request.session['last_cookie_set'] = request.session['last_visit']
            response = sign_cookie(response)
        return response
    return _add_signed_cookie
