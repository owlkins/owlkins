from django import template
from django.conf import settings


register = template.Library()

@register.filter
def possessive(name):
    if settings.LOCAL_CONFIG.get('BRITISH_POSSESSIVE'):
        return name + '\'s'
    elif name.endswith('s'):
        return name + '\''
    return name + '\'s'
