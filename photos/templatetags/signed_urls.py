from django import template
from datetime import datetime, timedelta
from botocore.signers import CloudFrontSigner
# https://cryptography.io as an RSA backend
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.serialization import (
    load_pem_private_key
)
from owlkins.vault import KEYS
from django.conf import settings
from django.utils import timezone

register = template.Library()

@register.filter
def keyvalue(dictionary, key):
    return dictionary.get(key)


@register.filter
def to_timestamp(dt):
    return dt.timestamp()

@register.filter
def is_new(uploaded_time, last_visit):
    """
    Return True for if the photo's "uploaded_time" is new, considering the last time the user visited, and also what
    was uploaded in the last day.
    :param uploaded_time:datetime.datetime
    :param last_visit:int
    :return:
    """
    return uploaded_time > _get_new_time(last_visit)

def _get_new_time(last_visit):
    if isinstance(last_visit, str):
        last_visit = float(last_visit)
    return min(timezone.now() - timedelta(days=1),
               timezone.datetime.fromtimestamp(last_visit).astimezone(timezone.get_current_timezone()))

CLOUDFRONT_SIGNER = None
if KEYS['CLOUDFRONT_PRIVATE_KEY']:
    KEY = load_pem_private_key(KEYS['CLOUDFRONT_PRIVATE_KEY'].encode('ascii'), password=None, backend=default_backend())
    CLOUDFRONT_SIGNER = CloudFrontSigner(KEYS['CLOUDFRONT_KEYPAIR_ID'], lambda x: KEY.sign(x, padding.PKCS1v15(), hashes.SHA1()))


@register.filter
def sign_url(url, expiration_seconds=3600, check_prod=True):
    if check_prod and settings.PROD:
        return url
    expiration = datetime.utcnow() + timedelta(seconds=expiration_seconds)
    # noinspection PyTypeChecker
    assert CLOUDFRONT_SIGNER, "secrets must contain a private key to sign cookies: CLOUDFRONT_PRIVATE_KEY"
    return CLOUDFRONT_SIGNER.generate_presigned_url(url, date_less_than=expiration)


@register.filter
def filename(url):
    return url.split("/")[-1]
