from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.frontpage, name='photos_main'),
    url(r'^photos_js_list', views.photos_js_list, name='photos_js_list'),
    url(r'check_last_update', views.last_upload_time, name='photos_last_upload_time'),
    url(r'upload', views.upload, name='photos_upload'),
    url(r'handle_file', views.handle_file, name='photos_handle_file'),
    url(r'cache_clear', views.cache_clear, name='photos_cache_clear'),
    url(r'available_months', views.available_months, name='photos_available_months'),
    url(r'photos_for_month', views.photos_for_month, name='photos_for_month'),
]
