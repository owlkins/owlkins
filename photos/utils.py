from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.files.base import ContentFile
from PIL import Image, ExifTags
from sentry_sdk import capture_exception


def try_rotate_image(inputData):

    try:
        image = Image.open(inputData)
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation] == 'Orientation':
                break
        exif = dict(image._getexif().items())

        if exif[orientation] == 3:
            image = image.rotate(180, expand=True)
        elif exif[orientation] == 6:
            image = image.rotate(270, expand=True)
        elif exif[orientation] == 8:
            image = image.rotate(90, expand=True)
        else:
            return inputData
        buffer = BytesIO()
        image.save(fp=buffer, format='JPEG')
        return InMemoryUploadedFile(ContentFile(buffer.getvalue()), None, inputData.name, 'image/jpeg', image.tell, None)

    except (AttributeError, KeyError, IndexError):
        # cases: image don't have getexif
        return inputData
    except Exception as e:
        capture_exception()
        return inputData
