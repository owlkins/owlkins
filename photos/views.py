from django.shortcuts import render
import logging
from django.http import HttpResponseRedirect
from django.contrib import messages
from .models import Photo
from django.http.response import JsonResponse
from .utils import try_rotate_image
from django.utils import timezone
from django.core.files import File
import datetime
from sentry_sdk import capture_exception
from PIL import Image
from .signed_cookies import add_signed_cookie
from django.core.cache import cache
import json
from django.contrib.auth.decorators import login_required
from photos.templatetags.signed_urls import sign_url, _get_new_time
from .convert_files import Convert2Mp4
from owlkins.env_context_processor import is_iOS
import pytz
from django.contrib.auth.models import User
from pathlib import Path
from django.conf import settings
import pickle
from django.db.models.functions import TruncMonth, ExtractYear
from django.db.models import F, CharField, Func
from django.db.models.functions import Concat
from django.db.models.expressions import Value
from django.template.defaultfilters import date as date_filter


logger = logging.getLogger(__name__)

COLORS = {
    'January': '#f0ece3',
    'February': '#f0f696',
    'March': '#96f7d2',
    'April': '#f5b5fc',
    'May': '#ddf3f5',
    'June': '#d4e683',
    'July': '#9dcada',
    'August': '#f3b8a9',
    'September': '#e97171',
    'October': '#f69e7b',
    'November': '#726a95',
    'December': '#383e56',
}
VIDEO_TEMPLATE = '<video controls style="display:block; margin-left:auto; margin-right:auto; padding-top: 40px;width:95%;height:95%;"><source src="{url}" type="video/mp4"></video>'

PHOTO_SUCCESS = 'photo-success'
VIDEO_SUCCESS = 'video-success'
UPLOAD_ERROR = 'upload-error'
CONVERSION_ERROR = 'conversion-error'

@login_required
@add_signed_cookie
def frontpage(request):
    iOS = is_iOS(request)
    _set_upload_success_message(request)
    photos, date_field = get_photos_for_request(request)

    class TrimmedMonthName(Func):
        function = 'TRIM'
        template = "%(function)s(TO_CHAR(%(expressions)s, 'Month'))"
    unique_months = photos.annotate(
        month=TruncMonth(date_field),
        year=ExtractYear(date_field),
    ).annotate(
        formatted_date=Concat(
            TrimmedMonthName(F('month')),
            Value(', '),
            F('year'),
            output_field=CharField()
        )
    ).values('formatted_date').distinct().order_by('-year', '-month')

    months = list(unique_months.values_list('formatted_date', flat=True))
    month_colors = {each: COLORS[each.split(", ")[0]] for each in months}

    users_for_filter = User.objects.filter(
            id__in=list(Photo.objects.filter(deleted=False).values_list('uploaded_by', flat=True).distinct())
        ).order_by('first_name')
    content = {
        'months': months,
        'month_colors': month_colors,
        'users_for_filter': users_for_filter,
        'last_visit': request.GET.get('last_visit') or request.session.get('last_visit', ''),
        'last_update': _last_upload_time(),
        'user_selected': int(request.GET.get('by_user')) if request.GET.get('by_user', '').isdigit() else None,
        'photos_present': photos.count() > 0,
        'new_only_checkbox': request.GET.get('new_only_checkbox'),
        'sort_by_select': request.GET.get('sort_by'),
        'iOS': iOS,
    }
    return render(request, 'photos/index.html', content)


def get_photos_for_request(request):
    date_field = 'uploaded_time' if request.GET.get('sort_by') == 'Uploaded Time' else 'created_time'
    photos = Photo.objects.filter(deleted=False).select_related('uploaded_by').order_by('-{}'.format(date_field))
    photos = _filter_by_new_time(request, photos)
    by_user = request.GET.get('by_user')
    if by_user and by_user.isdigit():
        photos = photos.filter(uploaded_by_id=by_user)
    return photos, date_field


def photos_js_list(request):
    iOS = is_iOS(request)
    photos, date_field = get_photos_for_request(request)
    cache_values = {**{
        'pending_count': photos.filter(image='videos/video_pending_processing.png').count(),
        'count': photos.count(),
        'by': date_field,
        'iOS': iOS,
    }, **{each: request.GET.get(each) for each in ['by_user', 'last_visit', 'new_only_checkbox']}}
    cache_key = 'photos-' + '-'.join(f'{key}={value}' for key, value in cache_values.items())
    results = cache.get(cache_key, default=[])
    if not results:
        for photo in photos:
            item = {
                'pid': photo.id,
                'month_year': getattr(photo, date_field).astimezone(timezone.get_current_timezone()).strftime('%B, %Y'),
                'title': _get_description(photo, date_field),
            }
            if photo.video:
                video_url = photo.video_ios.url if iOS and photo.video_ios else photo.video.url
                item.update({
                    'html': VIDEO_TEMPLATE.format(url=sign_url(video_url)),
                })
            else:
                item.update({
                    'src': sign_url(photo.image.url),
                    'w': photo.w,
                    'h': photo.h,
                })
            results.append(item)
        cache.set(cache_key, results, 60*60*12)  # 24 hr timeout
    return JsonResponse({
        'photos_js_list': results,
    })

def _filter_by_new_time(request, photos):
    if request.GET.get('new_only_checkbox') and (request.GET.get('last_visit') or request.session.get('last_visit')):
        new_time = _get_new_time(request.GET.get('last_visit') or request.session.get('last_visit'))
        photos = photos.filter(uploaded_time__gte=new_time)
    return photos

def _set_upload_success_message(request):
    cache_key_order = [PHOTO_SUCCESS, VIDEO_SUCCESS, UPLOAD_ERROR, CONVERSION_ERROR]
    cache_keys = ['{}-{}'.format(each, request.user.id) for each in cache_key_order]
    uploads = cache.get_many(cache_keys)
    photos_count = uploads.get(cache_keys[cache_key_order.index(PHOTO_SUCCESS)])
    videos_count = uploads.get(cache_keys[cache_key_order.index(VIDEO_SUCCESS)])
    success = '{photo}{and_included}{video}'.format(
        photo = '{n} new photo{s}'.format(n=photos_count, s='s' if photos_count > 1 else '') if photos_count else '',
        and_included = ' and ' if photos_count and videos_count else '',
        video = '{videos} video{s} under processing'.format(videos=videos_count, s='s' if videos_count > 1 else '') if videos_count else '',
    )
    if success:
        messages.success(request, 'Success! ' + success)
    upload_error = uploads.get(cache_keys[cache_key_order.index(UPLOAD_ERROR)])
    conversion_error = uploads.get(cache_keys[cache_key_order.index(CONVERSION_ERROR)])
    errors = '{upload}{and_included}{conversion}'.format(
        upload = '{n} failed to upload'.format(n=upload_error) if upload_error else '',
        and_included = ' and ' if upload_error and conversion_error else '',
        conversion = '{n} failed to process'.format(n=conversion_error) if conversion_error else '',
    )
    if errors:
        messages.error(request, 'Error: ' + errors)
    cache.delete_many(cache_keys)

def _last_upload_time():
    photos = Photo.objects.filter(deleted=False)
    if photos.count():
        return photos.order_by('-uploaded_time').first().uploaded_time.timestamp()


def last_upload_time(request):
    return JsonResponse({'last_update': _last_upload_time()})


def upload(request):
    if not request.user.is_authenticated:
        messages.info(request, 'You must login to continue')
        return HttpResponseRedirect('users/login')
    content = {}
    return render(request, 'photos/upload.html', content)


def handle_file(request):
    if not request.user.is_authenticated:
        messages.info(request, 'You must login to continue')
        return HttpResponseRedirect('users/login')
    if request.method == 'POST':
        name = request.POST.get('file_name', '')
        try:
            request_files = request.FILES
            file = request_files['file']
            timezone_offset = request.POST.get("timezone_offset")
            if timezone_offset:
                timezone_offset = pytz.FixedOffset(-int(timezone_offset))
            if not name:
                name = file.__dict__['_name']
        except Exception as e:
            if not any(each.level == messages.ERROR for each in messages.get_messages(request)):
                messages.error(request, 'Error uploading file %s' % name)
            capture_exception()
            _cache_incr(request, UPLOAD_ERROR)
            return JsonResponse({'success': False, }, status=501)
        try:
            if 'video' in file.content_type:
                tz_now = timezone.now()
                video = Photo(created_time=tz_now, uploaded_by=request.user, uploaded_time=tz_now)
                video.image.name = 'videos/video_pending_processing.png'
                video.save()
                if settings.VIDEO_DAEMON_SET_UP:
                    scratch_dir_for_id = Path(settings.VIDEO_SCRATCH_DIR, str(video.id))
                    scratch_dir_for_id.mkdir(parents=True, exist_ok=True)
                    with open(Path(scratch_dir_for_id, name), 'wb') as f:
                        f.write(file.read())
                    with open(Path(scratch_dir_for_id, 'meta'), 'wb') as f:
                        pickle.dump({
                            'timezone_offset': timezone_offset,
                            'filename': name
                        }, f)
                else:
                    Convert2Mp4(video,
                                filename=name, file=file, timezone_offset=timezone_offset,
                                ).start()
                _cache_incr(request, VIDEO_SUCCESS)
            else:
                img = try_rotate_image(file)
                created_time = timezone.now()
                try:
                    with Image.open(file) as f:
                        exif = f._getexif() or {}
                        exif_time = exif.get(36867) or \
                                       exif.get(306) or \
                                       exif.get(36867) or \
                                       exif.get(36868) or \
                                       getattr(f, 'info', {}).get('DateTime')
                    if exif_time:
                        created_time = datetime.datetime.strptime(exif_time, '%Y:%m:%d %H:%M:%S')
                        if hasattr(timezone_offset, 'localize'):
                            created_time = timezone_offset.localize(created_time)
                except Exception as e:
                    capture_exception()
                photo = Photo(created_time=created_time, uploaded_by=request.user, uploaded_time=timezone.now())
                photo.image.save(name, File(img))
                _cache_incr(request, PHOTO_SUCCESS)
            return JsonResponse({'success': True, })
        except Exception as e:
            if not any(each.level == messages.ERROR for each in messages.get_messages(request)):
                messages.error(request, 'Error uploading file %s' % name)
            capture_exception()
            _cache_incr(request, CONVERSION_ERROR)
            return JsonResponse({'success': False, }, status=500)
    messages.error(request, 'You can only POST to this endpoint.')
    return JsonResponse({'success': False, }, status=403)

def _cache_incr(request, key):
    cache_key = '{}-{}'.format(key, request.user.id)
    try:
        cache.incr(cache_key)
    except ValueError:
        cache.set(cache_key, 1, None)

def cache_clear(request):
    if request.user.is_staff:
        cache.clear()
        messages.success(request, 'Cache cleared')
    return HttpResponseRedirect('/')


@login_required
def available_months(request):
    return JsonResponse({
        'months': [(month.year, month.month,) for month in Photo.objects.filter(deleted=False).datetimes('created_time', kind='month')],
        'colors': COLORS,
    })


@login_required
def photos_for_month(request):
    month_year = request.GET.get('month_year')
    if month_year:
        month_year = datetime.datetime.strptime(month_year, '%B, %Y')
        month = month_year.month
        year = month_year.year
    else:
        month = request.GET.get('month') or datetime.date.today().month
        year = request.GET.get('year') or datetime.date.today().year
    date_field = 'uploaded_time' if request.GET.get('sort_by') == 'Uploaded Time' else 'created_time'
    kwargs = {
        '{date_field}__month__gte'.format(date_field=date_field): month,
        '{date_field}__month__lte'.format(date_field=date_field): request.GET.get('to_month') or month,
        '{date_field}__year__gte'.format(date_field=date_field): year,
        '{date_field}__year__lte'.format(date_field=date_field): request.GET.get('to_year') or year,
    }
    photos = Photo.objects.filter(
        deleted=False,
        **kwargs,
    ).order_by(f'-{date_field}')
    by_user = request.GET.get('by_user')
    if by_user and by_user.isdigit():
        photos = photos.filter(uploaded_by_id=by_user)
    photos = _filter_by_new_time(request, photos)
    return JsonResponse({
        'photos': [{
            'w': photo.w,
            'h': photo.h,
            'id': photo.id,
            'user': photo.uploaded_by.id,
            'user_name': photo.uploaded_by.first_name,
            'image_url': sign_url(photo.image.url) if photo.image else None,
            'thumbnail_url': sign_url(photo.image_thumbnail.url) if photo.image else None,
            'video_url': sign_url(photo.video.url) if photo.video else None,
            'uploaded_time': photo.uploaded_time.timestamp(),
            'date': getattr(photo, date_field).astimezone(timezone.get_current_timezone()).strftime('%b. %d, %Y, %I:%M %p'),
            'description': _get_description(photo, date_field=date_field),
        } for photo in photos]
    })

def _get_description(photo, date_field='created_time'):
    if photo.description:
        return photo.description
    return '[{id}] From: {user}, Date: {date}'.format(
        id=photo.id,
        user=photo.uploaded_by.first_name,
        date=getattr(photo, date_field).astimezone(timezone.get_current_timezone()).strftime('%b. %d, %Y, %I:%M %p'),
    )
