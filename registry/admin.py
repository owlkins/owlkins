from django.contrib import admin
from .models import Item


class ItemAdmin(admin.ModelAdmin):
    list_display = [each.name for each in Item._meta.fields]

admin.site.register(Item, ItemAdmin)