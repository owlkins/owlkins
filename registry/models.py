from django.db import models
from django.contrib.auth.models import User
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, Transpose

# Create your models here.
class Item(models.Model):
    """Base damage model"""
    name = models.CharField(max_length=256, null=True, blank=True, default=None)
    category = models.CharField(max_length=256, null=True, blank=True, default=None)
    description = models.CharField(max_length=2048, null=True, blank=True, default=None)
    url = models.CharField(max_length=5016)
    est_price = models.FloatField(null=True, blank=True)
    who_bought = models.CharField(max_length=120, blank=True, null=True, default=None)
    email_bought = models.CharField(max_length=256, null=True, blank=True, default=None)
    when_bought = models.DateTimeField(blank=True, null=True, default=None)
    image = models.ImageField(upload_to="registry", height_field='h', width_field='w', null=True, blank=True,
                              help_text='Optionally upload a photo to show your visitors.')
    image_thumbnail = ImageSpecField(source='image',
                                     processors=[
                                         Transpose(),
                                         ResizeToFill(328, 153),
                                     ],
                                     format='JPEG',
                                     options={'quality': 60})
    w = models.IntegerField(null=True, blank=True, help_text='width of photo in pixels, often set automatically on upload')
    h = models.IntegerField(null=True, blank=True, help_text='height of photo in pixels, often set automatically on upload')
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return '[{id}] {name} ({who_bought})'.format(
            id=self.id,
            name=self.name,
            who_bought=self.who_bought,
        )
