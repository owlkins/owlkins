from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.frontpage, name='registry_page'),
    url(r'registry_claim', views.registry_claim, name='registry_claim'),
]
