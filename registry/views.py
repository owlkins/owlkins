from django.shortcuts import render, Http404
import logging
from .models import Item
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.contrib import messages
from pytz import timezone as tz
import datetime
from django.conf import settings
from photos.signed_cookies import add_signed_cookie

logger = logging.getLogger(__name__)

@add_signed_cookie
def frontpage(request):
    items = Item.objects.filter(deleted=False).order_by('id')
    categories = {}
    colors = [
        '#add8e6',
        '#fafad2',
        '#b2ffb2',
        '#ffb6c1',
        '#e0ffff',
        '#f08080',
    ]
    colors += colors
    for item in items:
        if item.category not in categories:
            categories[item.category] = []
        categories[item.category].append(item)
    content = {
        'PARENT_NAMES': settings.LOCAL_CONFIG.get('PARENT_NAMES'),
        'PARENT_ADDRESS1': settings.LOCAL_CONFIG.get('PARENT_ADDRESS1'),
        'PARENT_ADDRESS2': settings.LOCAL_CONFIG.get('PARENT_ADDRESS2'),
        'PARENT_EMAIL': settings.LOCAL_CONFIG.get('PARENT_EMAIL'),
        'categories': categories,
        'colors': colors,
        'days_till_birth': (datetime.datetime(2020,8,1, tzinfo=tz('America/Chicago')) - timezone.now()).days
    }
    return render(request, 'registry/index.html', content)

def registry_claim(request):
    if request.method != 'POST':
        return Http404("This page is not available.")
    item = Item.objects.get(id=request.POST.get('itemID'))
    if item.when_bought:
        messages.error(request, 'That item was already claimed, sorry!')
        return HttpResponseRedirect('/')
    item.who_bought = request.POST.get('claim_first_name') + " " + request.POST.get('claim_last_name')
    item.email_bought = request.POST.get('claim_email')
    item.when_bought = timezone.now()
    item.save()
    messages.success(request, "Thanks! {name} claimed by you!".format(name=item.name))
    return HttpResponseRedirect('/')
