let form_submitted = false;
let in_modal = false;
function openPoopModal(id, el) {
    document.getElementById('poop_id').value = id;
    document.getElementById('poop-' + el.dataset.poop_type).click();
    document.getElementById('poop_start').value = el.dataset.start.slice(11, -6) || new Date().toTimeString().slice(0, 5);
    document.getElementById('poop_extra').value = el.dataset.extra;
    document.getElementById('poop_diaper_size').value = el.dataset.diaper_size;
    document.getElementById('poop_blowout').checked = (el.dataset.blowout === 'True');
    document.getElementById('poop_delete_button').style.display = '';
    in_modal = true;
    $('#poopModal').modal({})
}
function openSleepModal(id, el) {
    document.getElementById('sleep_id').value = id;
    document.getElementById('sleep_type').value = el.dataset.sleep_type;
    document.getElementById('sleep_start').value = el.dataset.start.slice(11, -6);
    document.getElementById('sleep_end').value = el.dataset.end.slice(11, -6);
    document.getElementById('sleep_extra').value = el.dataset.extra;
    document.getElementById('sleep_temperature').value = el.dataset.temperature;
    document.getElementById('sleep_delete_button').style.display = '';
    in_modal = true;
    $('#sleepModal').modal({})
}
function openFeedingModal(id, el) {
    document.getElementById('feeding_id').value = id;
    document.getElementById('feeding_type').value = el.dataset.feeding_type;
    document.getElementById('feeding_start').value = el.dataset.start.slice(11, -6) || new Date().toTimeString().slice(0, 5);
    document.getElementById('feeding_extra').value = el.dataset.extra;
    document.getElementById('feeding_amount').value = el.dataset.amount;
    document.getElementById('feeding_amount_in_oz').value = el.dataset.amount_in_oz;
    document.getElementById('feeding_length_of_feeding').value = el.dataset.length_of_feeding;
    document.getElementById('feeding_delete_button').style.display = '';
    in_modal = true;
    feedingTypeChange();
    $('#feedingModal').modal({})
}
let in_refresh = false;
async function refreshFunc() {
    if (!in_refresh && !form_submitted && !in_modal){
        in_refresh = true;
        try {
            let r = await fetch('/data/?check_last_update=1&' + window.location.search.replace("?", ""));
            let j = await r.json();
            if (j['last_update'] !== last_update) {
                document.getElementById('loading-cover').style.display = '';
                location.reload();
            }
        } catch (e) {
            Sentry.captureException(e)
        }
        in_refresh = false;
    }
}
window.addEventListener("load", (event) => {
    let offset = (new Date()).getTimezoneOffset().toString();
    ['poopModal', 'feedingModal', 'sleepModal'].forEach(function (modal) {
        let form = document.getElementById(modal).querySelector('form');
        form.addEventListener('submit', function (e) {
            form_submitted = true;
        })
        let offset_input = document.createElement('input');
        offset_input.type = 'hidden';
        offset_input.name = 'timezone_offset';
        offset_input.value = offset;
        form.appendChild(offset_input);
    })
    window.addEventListener('focus', refreshFunc);
    setInterval(refreshFunc, 7000);
    $("#poopModal").on('hidden.bs.modal', function () {
        document.getElementById('poop_id').value = '';
        let poop_type = document.getElementById('poop_type')
        poop_type.value = poop_type.dataset.default_entry;
        document.getElementById('poop-' + poop_smallest).click();
        document.getElementById('poop_start').value = '';
        document.getElementById('poop_extra').value = '';
        let diaper_size = document.getElementById('poop_diaper_size');
        diaper_size.value = diaper_size.dataset.default_entry;
        document.getElementById('poop_blowout').checked = false;
        document.getElementById('poop_delete_button').style.display = 'none';
        in_modal = false;
        refreshFunc();
    });
    $("#sleepModal").on('hidden.bs.modal', function () {
        document.getElementById('sleep_id').value = '';
        let sleep_type = document.getElementById('sleep_type')
        sleep_type.value = sleep_type.dataset.default_entry;
        document.getElementById('sleep_start').value = '';
        document.getElementById('sleep_end').value = '';
        document.getElementById('sleep_extra').value = '';
        document.getElementById('sleep_temperature').value = '';
        document.getElementById('sleep_delete_button').style.display = 'none';
        in_modal = false;
        refreshFunc();
    });
    $("#feedingModal").on('hidden.bs.modal', function () {
        document.getElementById('feeding_id').value = '';
        let feeding_type = document.getElementById('feeding_type')
        feeding_type.value = feeding_type.dataset.default_entry;
        document.getElementById('feeding_start').value = '';
        document.getElementById('feeding_extra').value = '';
        let amount_value = '';
        if (feeding_type.value === default_feeding_type && default_feeding_amount)
            amount_value = default_feeding_amount;
        document.getElementById('feeding_amount').value = amount_value;
        document.getElementById('feeding_length_of_feeding').value = '';
        document.getElementById('feeding_delete_button').style.display = 'none';
        in_modal = false;
        feedingTypeChange();
        refreshFunc();
    });
    $('#datepicker').datepicker({
        todayBtn: 'linked',
        todayHighlight: true,
    }).on('changeDate', function (ev) {
        let date = ev.date.toDateString();
        window.location = '/data/?date=' + date;
    });
})
function auto_populate_time(tag) {
    document.getElementById(tag).value = new Date().toTimeString().slice(0, 5);
}
function initiateDelete(kind) {
    let button = document.getElementById(kind + '_confirm_button');
    button.innerHTML = '<div class="spinner-border text-light" style="height:1em;width:1em;margin-left:42.5px;margin-right:42.5px"></div>';
    disableAllButtons();
    let form = document.getElementById(kind + 'Modal').querySelector('form');
    let confirm_delete = document.createElement('input');
    confirm_delete.type = 'hidden';
    confirm_delete.name = 'delete';
    confirm_delete.value = 'confirmed';
    form.appendChild(confirm_delete);
    form_submitted = true;
    form.submit();
}
$(document).on('show.bs.modal', '.modal', function() {
  const zIndex = 1040 + 10 * $('.modal:visible').length;
  $(this).css('z-index', zIndex);
  setTimeout(() => $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack'));
});