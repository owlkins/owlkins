let observer;
let options = {
    root: null,
    rootMargin: "0px",
    threshold: [1]
};
observer = new IntersectionObserver(handleIntersect, options);
function handleIntersect(entries, observer) {
  entries.forEach((entry) => {
      if (entry.target.classList.contains('loading-placeholder')) loadMonth(entry.target)
  });
}
let MONTH_LOCKED = [];
function loadMonth(div) {
    let parent = div.closest('.row');
    if (div.closest('.card-body').className.includes('show')) {
        if (MONTH_LOCKED.includes(div.dataset.month_year)) return;
        MONTH_LOCKED.push(div.dataset.month_year);
        let month_year = div.dataset.month_year;
        fetch('/photos/photos_for_month' + (document.location.search || '?') +'&month_year=' + month_year)
            .then(response => {
                if (!response.ok) {
                    document.getElementById('warning-for-loading-placeholder-' + month_year).style.display = '';
                    setTimeout(() => {
                        MONTH_LOCKED.pop(MONTH_LOCKED.indexOf(div.dataset.month_year));
                        loadMonth(div);
                    }, 10000);
                    return {'error': response}
                }
                return response.json();
            })
            .then(data => {
                document.getElementById('warning-for-loading-placeholder-' + month_year).style.display = 'none';
                data.photos.forEach((photo) => {createPhotoDiv(photo, parent)});
                div.style.display = 'none';
                div.classList.remove('loading-placeholder');
                observer.unobserve(div);
            }).catch((error) => {
                document.getElementById('warning-for-loading-placeholder-' + month_year).style.display = '';
                setTimeout(() => {
                    MONTH_LOCKED.pop(MONTH_LOCKED.indexOf(div.dataset.month_year));
                    loadMonth(div);
                }, 10000);
        });
    }
}
function createPhotoDiv(photo, parent) {
    let col = document.createElement('div');
    col.className = 'col-12 col-md-4';
    let card = document.createElement('div');
    card.className = 'card photo-card';
    card.onclick = function(){openPhotoSwipe(id=photo.id)};
    let img = document.createElement('img');
    img.className = 'card-img-top photo-img';
    img.id = photo.id;
    img.loading='lazy';
    img.alt='Card image cap';
    img.src=photo.thumbnail_url;
    card.appendChild(img);
    let time_to_compare = Math.min(Math.floor(new Date().getTime() / 1000) - 24*3600, last_visit);
    if (last_visit && (photo.uploaded_time > time_to_compare)) {
        let new_ribbon = document.createElement('div');
        new_ribbon.className = 'corner-ribbon top-left red shadow'
        new_ribbon.innerText = 'New'
        card.appendChild(new_ribbon);
    }
    if (photo.user === user) {
        let owner_ribbon = document.createElement('div');
        owner_ribbon.className = 'corner-ribbon top-right yellow shadow';
        owner_ribbon.innerText = 'Yours';
        card.appendChild(owner_ribbon);
    }
    let card_body = document.createElement('div');
    card_body.className = 'card-body photo-text';
    let card_text = document.createElement('p');
    card_text.className = 'card-text';
    let text = ''
    if (photo.user_name) {
        text += 'From: ' + photo.user_name + ', '
    }
    text += photo.date;
    card_text.innerText = text;
    card_body.appendChild(card_text);
    card.appendChild(card_body);
    col.appendChild(card);
    parent.appendChild(col);
}
window.addEventListener("load", (event) => {
    Array.from(document.getElementsByClassName('loading-placeholder')).forEach((div) => {
        observer.observe(div);
    })
    $('.card-body.collapse').on('shown.bs.collapse', function () {
        Array.from(document.getElementsByClassName('loading-placeholder')).forEach(loadMonth);
    })
}, false);