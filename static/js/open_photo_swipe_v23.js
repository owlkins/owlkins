let after_hash_change = false;
let just_closed = false;
let photos_loaded = false;
let previous_index;
let gallery;
let photo_items = [];
function removePhotoswipeLoading () {
    Array.from(document.getElementsByClassName('photoswipe-loading')).forEach((el) => {
    el.style.display = 'none';
})}
function openPhotoSwipe(id = 0) {
    if (photos_loaded) {
        just_closed = false;
        let current_index = photo_items.findIndex((el) => el.pid === id);
        previous_index = current_index;
        var pswpElement = document.querySelectorAll('.pswp')[0];
        var options = {
            index: current_index,
            isClickableElement: function(el) {
                return el.tagName === 'A' || el.tagName === 'IMG';
            },
            loop: false,
            shareButtons: [
                {
                    id: 'download',
                    label: 'Download image',
                    url: '{{raw_image_url}}',
                    download: true
                }
            ],
            galleryPIDs: true,
        };
        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, photo_items, options);
        gallery.init();
        gallery.listen('afterChange', preventBadMove);
        gallery.listen('afterChange', stopVideos);
        gallery.listen('close', stopVideos);
        gallery.listen('close', scroll_to_thumbnail);
        gallery.listen('close', function () { just_closed = true; })
        gallery.listen('destroy', function() { gallery = null; });
    } else {
        $('#photos_loading_toast').toast('show');
    }
}
function scroll_to_thumbnail() {
    let photo = photo_items[gallery.getCurrentIndex()];
    let month_year = photo['month_year'];
    if (!MONTH_LOCKED.includes(month_year)) {
        let month_card = document.getElementById(month_year)
        month_card.scrollIntoView({block: "center"});
        month_card.click();
    }
    select_and_scroll_to_image(photo['pid']);
}
function select_and_scroll_to_image(pid, attempt=0) {
    let photo_div = document.getElementById(pid);
    if (photo_div === null) {
        if (attempt > 100) {
            return;
        }
        setTimeout(()=>{select_and_scroll_to_image(pid, attempt + 1)}, 50)
    } else {
        let parent_element = photo_div.parentElement;
        parent_element.style.boxShadow = '0 0 10px 5px #ffbc00';
        scrollIntoViewIfNeeded(parent_element);
        // "Fade" out
        setTimeout(()=>{
            parent_element.style.boxShadow = '0 0 10px 5px #ffbc008f'
            setTimeout(()=>{
                parent_element.style.boxShadow = '0 0 10px 5px #ffbc003d'
                setTimeout(()=>{
                    parent_element.style.boxShadow = ''
                }, 250)
            }, 250)
        },4000);
    }
}
function preventBadMove() {
    if (after_hash_change && previous_index !== 0 && gallery.getCurrentIndex() === 0) {
        gallery.goTo(previous_index);
        after_hash_change = false;
    }
}
function stopVideos () {
    let prev_item = gallery.getItemAt(previous_index)
    if (prev_item && 'html' in prev_item && prev_item.container) {
        let element = gallery.getItemAt(previous_index).container.firstElementChild;
        if ('VIDEO' === element.tagName) {
            element.pause();
        }
    }
    previous_index = gallery.getCurrentIndex();
}
let gallery_loading_toast_shown = false;
function open_hash_pid (attempt = 0) {
    let selected_photo = window.location.hash.split("pid=")[1];
    if (selected_photo && !just_closed) {
        if (!photos_loaded) {
            // Only call 'show' once and delay a slight bit at initial page load
            if (!gallery_loading_toast_shown && attempt > 2) {
                document.getElementById('gallery_loading_toast_id').innerText = `photo [${selected_photo}]`;
                $('#gallery_loading_toast').toast('show');
                gallery_loading_toast_shown = true;
            }
            // Retry (recursively)
            setTimeout(()=>{open_hash_pid(attempt + 1)}, 50)
            return;
        }
        // Hide (with some delay for any pending opening animation)
        setTimeout(()=>{$('#gallery_loading_toast').toast('hide');}, 100);
        selected_photo = parseInt(selected_photo);
        if (gallery) {
            let current_index = photo_items.findIndex((el) => el.pid === selected_photo);
            previous_index = current_index;
            gallery.goTo(current_index);
            after_hash_change = true;
        } else {
            openPhotoSwipe(selected_photo)
        }
    }
}
function load_photos_js_list() {
    fetch(window.location.pathname + 'photos_js_list' + window.location.search).then((response) => {
        if (response.ok)
            return response.json();
    }).then((data) => {
        photo_items = data['photos_js_list'];
        photos_loaded = true;
        $('#photos_loading_toast').toast('hide');
    })

}
window.addEventListener("load", (event) => {
    load_photos_js_list();
    open_hash_pid();
    removePhotoswipeLoading();
}, false);
window.onhashchange = open_hash_pid;