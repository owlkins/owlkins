window.addEventListener("load", (event) => {
    let buttons = document.getElementsByClassName('btn-submit');
    Array.from(buttons).forEach((button) => button.onclick = btnLoading(button));
})
let in_submit = false;
function btnLoading(button) {
    return (event) => {
        if (!in_submit) {
            let form = button.closest('form');
            if (form.checkValidity()) {
                in_submit = true;
                button.style.height = button.offsetHeight + 'px';
                button.style.width = button.offsetWidth + 'px';
                button.innerHTML = '<div class="spinner-border text-light" style="height:1em;width:1em;"></div>';
                disableAllButtons();
                Array.from(document.getElementsByTagName("button")).forEach((button) => {button.disabled = true; })
                let modal_content = button.closest('.modal-content');
                if (modal_content)
                    modal_content.children[0].children[1].disabled=true;
                form.submit();
            }
        }
    }
}
function disableAllButtons() {
    Array.from(document.getElementsByTagName("button")).forEach((button) => {button.disabled = true; });
}