function windowLocationChange(field) {
    return function (ev) {
        let date = ev.date.toDateString();
        let search = '';
        window.location.search.split("&").forEach((s) => {
            if (!s.split("=").includes(field)) {
                search += s.replace("?", "") + '&';
            }
        });
        search += field + '=' + date;
        window.location = window.location.pathname + '?' + search;
    }
}
window.addEventListener("load", (event) => {
    $('#start_date').datepicker({
        format: 'yyyy-mm-dd',
    }).datepicker('setDate', start_date).on('changeDate', windowLocationChange('start'));
    $('#end_date').datepicker({
        format: 'yyyy-mm-dd',
    }).datepicker('setDate', end_date).on('changeDate', windowLocationChange('end'));
    $('.dropdown-menu').on("click.bs.dropdown", function (e) {
        e.stopPropagation();
        e.preventDefault();
    })
});