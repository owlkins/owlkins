    let dropArea = document.getElementById('drop-area');
    let filesDone = 0;
    let filesToDo = 0;
    let progressBar = document.getElementById('upload-bar');
    let finishedBar = document.getElementById('finished-bar');
    let csrftoken = document.getElementsByName('csrfmiddlewaretoken')[0].value;
    let inAsync = false;
    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, preventDefaults, false);
    });

    function preventDefaults(e) {
        e.preventDefault()
        e.stopPropagation()
    }

    ['dragenter', 'dragover'].forEach(eventName => {
        dropArea.addEventListener(eventName, highlight, false)
    });

    ['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, unhighlight, false)
    })

    function highlight(e) {
        dropArea.classList.add('highlight')
    }

    function unhighlight(e) {
        dropArea.classList.remove('highlight')
    }

    dropArea.addEventListener('drop', handleDrop, false)

    function handleDrop(e) {
        let dt = e.dataTransfer
        let files = dt.files

        if (~inAsync) {
            handleFiles(files);
        }
    }

    function handleFiles(files) {
        filesToDo = files.length;;
        document.getElementById('fileElem').style.color = 'grey';
        document.getElementById('fileElem').disabled = true;
        document.getElementById('drop-area').style.backgroundColor = '#e4e4e4';
        document.getElementById('drop-area').style.color = 'grey';
        files = [...files]
        initializeProgress(files.length)
        files.forEach( (file, i) => setTimeout(() => { uploadFile(file, i); }, 200));
        // files.forEach(previewFile)
    }

    function previewFile(file) {
        if (file.type.includes('image')) {
            let reader = new FileReader()
            reader.readAsDataURL(file)
            reader.onloadend = function () {
                let img = document.createElement('img')
                img.src = reader.result
                document.getElementById('gallery').appendChild(img)
            }
        }
    }

    let failed_files = 0;

    function uploadFile(file, i) { // <- Add `i` parameter
        var url = '/photos/handle_file'
        var formData = new FormData()
        formData.append('file', file,);
        formData.append('file_name', file.name,);
        formData.append('timezone_offset', (new Date()).getTimezoneOffset().toString());
        var xhr = new XMLHttpRequest()
        xhr.open('POST', url, true)

        // Add following event listener
        xhr.upload.addEventListener("progress", function (e) {
            updateProgress(i, (e.loaded * 100.0 / e.total) || 100)
        })

        xhr.addEventListener('readystatechange', function (e) {
            if (filesDone === 0) {
                Array.from(document.getElementsByClassName('finished-bar')).forEach((elem) => {
                    elem.style.display = '';
                })
            }
            if (xhr.readyState === 4 && xhr.status === 200) {
                filesDone += 1;
                try {
                    finishedBar.value = (filesDone / filesToDo * 100) || 100;
                } catch (e) {
                    Sentry.captureMessage('finishedBar.value: ' + finishedBar.value + '\nfilesDone: ' + filesDone + '\nfilesToDo: ' + filesToDo);
                }
            } else if (xhr.readyState === 4 && xhr.status === 501 && failed_files < 50) {
                Sentry.captureMessage(xhr.status + ': Problem uploading file ' + file);
                failed_files += 1;
                document.getElementById('upload-warning').style.display = '';
                setTimeout(() => { uploadFile(file, i); }, 500);
            } else if (xhr.readyState === 4) {
                Sentry.captureMessage(xhr.status + ': Problem uploading file ' + file);
                filesDone += 1;
                document.getElementById('upload-fail').style.display = '';
            }
            if (filesDone === filesToDo) {
                setTimeout(() => { window.location.href = '/'; }, 2000);
            }
        })

        xhr.setRequestHeader("X-CSRFToken", csrftoken);
        xhr.send(formData);
    }

    let uploadProgress = [];

    function initializeProgress(numFiles) {
        progressBar.value = 0
        uploadProgress = []
                Array.from(document.getElementsByClassName('upload-bar')).forEach((elem) => {
                    elem.style.display = '';
                })
        document.getElementById('spinner-row').style.display = '';
        for (let i = numFiles; i > 0; i--) {
            uploadProgress.push(0)
        }
    }

    function updateProgress(fileNumber, percent) {
        uploadProgress[fileNumber] = percent
        let total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length
        progressBar.value = total
    }
    let offset = (new Date()).getTimezoneOffset();
