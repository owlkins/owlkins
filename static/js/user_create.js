function copy_URL() {
    /* Get the text field */
    var copyText = document.getElementById("verification_url");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");
    document.getElementById('copy_button').innerText = 'Copied!';
}