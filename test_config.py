from owlkins.config import LOCAL_CONFIG
from owlkins.config_defaults import LOCAL_CONFIG as DEFAULT_CONFIG
from owlkins.vault import KEYS
from owlkins.secrets_defaults import KEYS as DEFAULT_KEYS
import logging

logger = logging.getLogger('test_config')

def test_config():
    required = [
        'ADMIN_EMAIL',
        'DEFAULT_EMAIL_DISPLAY',
        'EMAIL_HOST',
        'EMAIL_DOMAIN',
        'BABY_NAME',
        'BASE_URL',
        'PARENT_NAMES',
    ]
    registry_specific = [
        'PARENT_ADDRESS1',
        'PARENT_ADDRESS2',
        'PARENT_EMAIL',
    ]
    photos_specific = [
        'AWS_STORAGE_BUCKET_NAME',
        'AWS_CLOUDFRONT_DOMAIN',
        'ARN',
    ]
    warnings = 0
    errors = []
    for key, default in DEFAULT_CONFIG.items():
        value = LOCAL_CONFIG.get(key)
        missing = (value is None or value == '')
        if missing and value in registry_specific and not LOCAL_CONFIG.get('HIDE_REGISTRY'):
            logger.warning("config.py missing {} but Registry is set to show".format(key))
            warnings += 1
        elif missing and value in photos_specific and not LOCAL_CONFIG.get('HIDE_PHOTOS'):
            logger.warning("config.py missing {} but Photos is set to show".format(key))
            warnings += 1
        elif missing and key in required:
            logger.warning("config.py missing {}".format(key))
            errors.append("{} missing but required.".format(key))
        elif value is not None and type(value) is not type(default):
            errors.append("{key} in config.py has value {value} which is type {t1} but it should be {t2}".format(
                key=key,
                value=value,
                t1=type(value),
                t2=type(default),
            ))

    if errors:
        logger.critical('\n    config.py FAILS checks')
        for error in errors:
            logger.error(error)
    else:
        logger.critical('\n    config.py passes checks{}'.format(' with {} warnings'.format(warnings) if warnings else ''))


def test_secrets():
    missing = set(DEFAULT_KEYS) - set(KEYS)
    if missing:
        logger.error('{} are missing from your secrets store'.format(missing))
    else:
        logger.critical('\n    secrets store passes checks')

if __name__ == '__main__':
    test_config()
    test_secrets()
