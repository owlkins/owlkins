from django.contrib import admin
from .models import Verification, EmailDigest


class VerificationAdmin(admin.ModelAdmin):
    list_display = [each.name for each in Verification._meta.fields]

class EmailDigestAdmin(admin.ModelAdmin):
    list_display = [each.name for each in EmailDigest._meta.fields]

admin.site.register(Verification, VerificationAdmin)
admin.site.register(EmailDigest, EmailDigestAdmin)
