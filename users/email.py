from django.conf import settings
import os
from django.core.mail import EmailMultiAlternatives
import re
import threading
from django.conf import settings
import logging
from sentry_sdk import capture_exception
from photos.templatetags.names import possessive


logger = logging.getLogger(__name__)

tagREMOVE = re.compile(r'\<\/?[a-zA-Z0-9]*\>')


class BaseEmail(threading.Thread):
    """
    An email class that reads in the email template file and replaces fields:
         {{greeting}}, {{firstname}}, {{message}}, {{endGreeting}}, {{endSig}}

    Optional arguments with defaults:
        template = 'contact/templates/baseEmailTemplate.html'
        subject = ''
        fromAddr = settings.DEFAULT_FROM_EMAIL
        toAddr = []
        greeting = ''
        name = ''
        body = ''
        baseMessage = ''
        endGreeting = 'Sincerely'
        endSig = 'Austin & Erna
        sendOnWarning = False
        sendToMessages = True
        unsubscribeURL = Optional string to provide in footer describing opt out, can include link.
    """

    def __init__(self, **kwargs):
        f = open(settings.BASE_DIR + os.sep + kwargs.get('template', 'users/templates/baseEmailTemplate.html'))
        self.htmlTemplate = f.read()
        f.close()
        self.subject = kwargs.get('subject', '')
        self.fromAddr = kwargs.get('fromAddr', settings.DEFAULT_FROM_EMAIL)
        self.greeting = kwargs.get('greeting', '')
        self.body = kwargs.get('body', '')
        self.baseMessage = kwargs.get('baseMessage', '')
        self.endGreeting = kwargs.get('endGreeting',
                                      'Sincerely,' if 'PARENT_NAMES' in settings.LOCAL_CONFIG else '')
        self.endSig = kwargs.get('endSig', settings.LOCAL_CONFIG.get('PARENT_NAMES', ''))
        self.sendOnWarning = kwargs.get('sendOnWarning', False)
        self.sendToMessages = kwargs.get('sendToMessages', True)
        self.user = kwargs.get('user')
        self.toAddr = kwargs.get('toAddr', [])
        self.userFirstName = kwargs.get('name', '')
        self.uniqueKey = kwargs.get('uniqueKey', '')
        self.opt_out_message = kwargs.get('opt_out_message', '')
        if self.user:
            self.toAddr = self.user.email
            self.userFirstName = self.user.first_name
        threading.Thread.__init__(self)

    def getHTMLMessage(self):
        html_msg = self.htmlTemplate.replace('{{ greeting }}', self.greetingCombo())
        html_msg = html_msg.replace('{{ message }}', self.body.replace('\n', '<br>'))
        html_msg = html_msg.replace('{{ endGreeting }}', self.endGreeting)
        html_msg = html_msg.replace('{{ endSig }}', self.endSig.replace('\n', '<br>'))
        html_msg = html_msg.replace('{{ opt_out_message }}', self.opt_out_message)
        html_msg = html_msg.replace('{{ BABY_NAME_POSSESSIVE }}', possessive(settings.LOCAL_CONFIG['BABY_NAME']))
        html_msg = html_msg.replace('{{ BASE_URL }}', settings.LOCAL_CONFIG['BASE_URL'])
        html_msg = html_msg.replace('{{ COPYRIGHT_MESSAGE }}',
                                    'Generated using <a href="https://owlkins.com">Owlkins</a>.' if settings.LOCAL_CONFIG.get('SHOW_ATTRIBUTION') else '')
        return html_msg

    def greetingCombo(self):
        return '%s%s%s%s' % (self.greeting,
                             ' ' if self.greeting and self.userFirstName else '',
                             self.userFirstName,
                             ',' if self.greeting or self.userFirstName else '')

    def getMessage(self):
        """ Non-HTML version of the message."""
        if self.baseMessage:
            # This does not handle complex tags like <a href="...."> but will
            # handle </a>.
            # \<\/?[a-zA-Z0-9\s]*(href=)?'?"?(?P<link>[\w])'?"?\>
            return tagREMOVE.sub('', self.baseMessage)
        return "%s\n\n%s\n\n%s\n%s" % (self.greetingCombo(),
                                       self.body,
                                       self.endGreeting,
                                       self.endSig,)

    def sendMessage(self):
        if settings.TESTING:
            self.run()
        else:
            self.start()

    def run(self):
        try:
            self._sendMessage()
        except Exception as e:
            logger.critical("Problem sending email: %s" % str(e))
            capture_exception()

    def _sendMessage(self):
        if not all([self.subject, self.body, self.fromAddr, self.toAddr, ]):
            if not settings.TESTING:
                logger.warning("WARNING on email send: %s fields blank." % ', '.join([item for item in ['subject',
                                                                                                        'body',
                                                                                                        'fromAddr',
                                                                                                        'toAddr'] if
                                                                                      not getattr(self, item)]))
            if not self.sendOnWarning: return False
        msg =  EmailMultiAlternatives(
            subject=self.subject,
            body=self.getMessage(),
            from_email=self.fromAddr,
            to=self.toAddr if isinstance(self.toAddr, list) else [self.toAddr],
            reply_to=[settings.LOCAL_CONFIG['ADMIN_EMAIL']],
        )
        msg.attach_alternative(self.getHTMLMessage(), "text/html")
        msg.send()
