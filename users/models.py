from django.db import models
from django.contrib.auth.models import User


class Verification(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    kind = models.CharField(max_length=40, null=True, blank=True)
    key = models.CharField(max_length=1028, null=True, blank=True)
    key_expires = models.DateTimeField(null=True, blank=True)
    key_used = models.DateTimeField(null=True, blank=True)
    extra =  models.CharField(max_length=512, null=True, blank=True)

    def __str__(self):
        return '[{id}] {kind}'.format(id=self.id, kind=self.kind)


class EmailDigest(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    opted_out = models.BooleanField(default=False, null=True, blank=True)
    opted_out_time = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '[{id}] {kind}'.format(id=self.id, kind=self.opted_out)
