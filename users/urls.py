from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login', views.user_login, name='user_login'),
    url(r'^logout', views.user_logout, name='user_logout'),
    url(r'^forgot_password', views.forgot_password, name='user_forgot_password'),
    url(r'^reset_password', views.reset_password, name='user_reset_password'),
    url(r'^confirm', views.message_only, name='user_confirm'),
    url(r'^create', views.user_create, name='user_create'),
    url(r'^set_password', views.set_password, name='user_set_password'),
    url(r'^email_digest_opt_out', views.email_digest_opt_out, name='email_digest_opt_out'),
]
