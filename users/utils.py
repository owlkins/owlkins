from .models import Verification
from django.utils.crypto import get_random_string
import datetime
import hashlib


def generate_verification_key(username=''):
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    secret_key = get_random_string(64, chars)
    return hashlib.sha256((secret_key + username).encode('utf-8')).hexdigest()


def get_verification_key(user, kind, extra=None, expiration_days=2):
    activation_key = generate_verification_key(user.username)
    key_expires = datetime.datetime.now() + datetime.timedelta(days=expiration_days)
    ver = Verification(user=user,
                       kind=kind,
                       key=activation_key,
                       key_expires=key_expires,
                       extra=extra)
    ver.save()
    return activation_key
