from django.shortcuts import render
import logging
from django.http import HttpResponseRedirect
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.models import User
from django.contrib import messages
from owlkins.settings import BASE_URL
from .email import BaseEmail
from sentry_sdk import capture_exception
from django.conf import settings
from django.contrib.auth import logout, authenticate, login
from .utils import get_verification_key
from .models import Verification, EmailDigest
import datetime
import pytz
from photos.templatetags.names import possessive


logger = logging.getLogger(__name__)


def user_login(request):
    content = {
    }
    if request.method == 'POST':
        email = request.POST.get('email').lower().strip()
        password = request.POST.get('password').strip()
        user = authenticate(username=email, password=password)
        if user and user.is_active:
            login(request, user)
            redirect = request.GET.get('next', '')
            return HttpResponseRedirect('/' if not redirect.startswith('/') else '' + redirect)
        messages.error(request, 'Login credentials are invalid.')
        return HttpResponseRedirect('/users/login')
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')
    return render(request, 'users/login.html', content)


def forgot_password(request):
    content = {
    }
    if request.method == 'POST':
        email = request.POST.get('email')
        users = User.objects.filter(email=email.lower())
        if not (users and users[0].is_active):
            messages.error(request, "No account found for the given input.")
            return HttpResponseRedirect('/users/forgot_password')
        user = users[0]
        try:
            email = BaseEmail(subject='Your forgot password request.',
                              user=user,
                              body=_forgot_message(user),
                              greeting='Hi'
                              )
            email.sendMessage()
        except Exception as e:
            capture_exception()
            logging.error('Error %s on forgotpassword email.' % e)
        messages.success(request, "An email has been sent with further instructions.")
        return HttpResponseRedirect('/users/confirm')
    return render(request, 'users/forgot_password.html', content)


def _forgot_message(user):
    paragraph = "We were notified that you requested to reset your password for your account on {baby_name_possessive} server.\n" \
                "If this was not you, please disregard this message.\n\n" \
                "To reset your password, click the following link:\n" \
                "<a href=\"https://{base_url}/users/reset_password?" \
                "uid={uid}&token={token}\">Password Reset Link</a>".format(
        baby_name_possessive=possessive(settings.LOCAL_CONFIG['BABY_NAME']),
        token=default_token_generator.make_token(user),
        uid=user.email,
        base_url=BASE_URL)
    return paragraph


def _set_password(user, creator, key):
    paragraph = "{creator} would like to share photos of {baby_name} and is sending you a link to set a " \
                "password which you can use to access the server.\n\n" \
                "To set your password, click the following link:\n\n" \
                "<a href=\"https://{base_url}/users/set_password?key={key}\">Set Password</a>".format(
        baby_name=settings.LOCAL_CONFIG['BABY_NAME'],
        creator=creator or 'We',
        key=key,
        base_url=BASE_URL)
    return paragraph


def reset_password(request):
    content = {}
    if request.method == 'POST':
        pw1 = request.POST.get('password').strip()
        pw2 = request.POST.get('confirm_password').strip()
        if pw1 != pw2:
            messages.error(request, "The password and confirmation don't match please try again.")
            return HttpResponseRedirect('/users/reset_password?{query}'.format(query=request.GET.urlencode()))
        user = User.objects.filter(email=request.GET.get('uid').lower())
        token = request.GET.get('token')
        if not (user and user[0].is_active):
            messages.error("User account not found.")
            return HttpResponseRedirect('/users/confirm')
        elif user and default_token_generator.check_token(user[0], token):
            user = user[0]
            user.set_password(pw1)
            user.save()
            paragraph = 'The password for your account on {baby_name_possessive} server was successfully reset.'.format(
                baby_name_possessive=possessive(settings.LOCAL_CONFIG['BABY_NAME']),
            )
            paragraph += '\nSign in with your new password by visiting the '
            paragraph += '<a href="' + settings.BASE_URL + '/users/login">Login Page</a>.\n'
            paragraph += 'If you believe you received this message in error, please '
            paragraph += '<a href="mailto:{ADMIN_EMAIL}">contact us</a> immediately.'.format(
                ADMIN_EMAIL=settings.LOCAL_CONFIG.get('ADMIN_EMAIL'),
            )
            try:
                email = BaseEmail(subject='Your password has been changed.',
                                  toAddr=user.username,
                                  body=paragraph,
                                  name=user.first_name,
                                  greeting='Hi'
                                  )
                email.sendMessage()
            except Exception as e:
                logging.error('Error %s on resetpassword email.' % e)
            messages.success(request, "Password changed successfully.")
            return HttpResponseRedirect('/users/login')

    return render(request, 'users/reset_password.html', content)


def message_only(request):
    return render(request, 'users/message_only.html', {})


def user_logout(request):
    logout(request)
    messages.success(request, 'Logged out')
    return HttpResponseRedirect('/users/login')


def user_create(request):
    if not (request.user.is_authenticated and request.user.is_staff):
        messages.info(request, 'You must be an administrator to continue')
        return HttpResponseRedirect('/users/login?next=users/create')
    if request.method == 'POST':
        email = request.POST.get('email').lower().strip()
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        expiration = int(request.POST.get('expiration', 2))
        send_email = request.POST.get('send_email')
        digest_opt_out = request.POST.get('digest_opt_out')
        if User.objects.filter(email=email):
            messages.error(request, 'User with email [{email}] already exists!'.format(email=email))
            return HttpResponseRedirect('/users/create')
        user = User(username=email, email=email, first_name=first_name, last_name=last_name)
        user.save()
        key = get_verification_key(user, kind='user_creation', expiration_days=expiration)
        if send_email:
            try:
                email = BaseEmail(subject='Join us in sharing photos of %s!' % settings.LOCAL_CONFIG['BABY_NAME'],
                                  user=user,
                                  body=_set_password(user, request.user.first_name, key),
                                  greeting='Hi'
                                  )
                email.sendMessage()
                messages.success(request, 'An email was sent to {email}'.format(email=user.first_name))
            except Exception as e:
                capture_exception()
                logging.error('Error %s on forgotpassword email.' % e)
        else:
            request.session['verification_key'] = key
        if digest_opt_out:
            EmailDigest(user=user, opted_out=True).save()
        return HttpResponseRedirect('/users/create')
    content = {}
    if request.session.get('verification_key'):
        content['verification_url'] = 'https://{base_url}/users/set_password?key={key}'.format(
            base_url=settings.BASE_URL, key=request.session['verification_key'])
        request.session['verification_key'] = None
    return render(request, 'users/user_create.html', content)


def set_password(request):
    verification = _get_verification_key(request, 'user_creation')
    if not isinstance(verification, Verification):
        return verification
    content = {
        'email': verification.user.email
    }
    if request.method == 'POST':
        pw1 = request.POST.get('password').strip()
        pw2 = request.POST.get('password_confirm').strip()
        if pw1 != pw2:
            messages.error(request, 'Password and Confirm Password fields must match')
            return HttpResponseRedirect('/users/set_password?key={key}'.format(key=request.GET.get('key')))
        verification.user.set_password(pw1)
        verification.user.save()
        verification.key_used = datetime.datetime.now()
        verification.save()
        messages.success(request, 'Password successfully set!')
        login(request, verification.user)
        return HttpResponseRedirect('/')
    return render(request, 'users/set_password.html', content)

def _get_verification_key(request, kind, return_key_used=False):
    verification = Verification.objects.filter(key=request.GET.get('key'), kind=kind)
    if not verification:
        messages.error(request, 'No such key')
        return render(request, 'users/message_only.html', {})
    verification = verification[0]
    if verification.key_used and return_key_used:
        return verification
    elif verification.key_used and not return_key_used:
        messages.error(request, 'That link has already been used!')
        return render(request, 'users/message_only.html', {})
    elif verification.key_expires < datetime.datetime.utcnow().replace(tzinfo=pytz.utc):
        messages.error(request, 'Key is no longer valid.')
        return render(request, 'users/message_only.html', {})
    return verification

def email_digest_opt_out(request):
    verification = _get_verification_key(request, 'email_digest_opt_out', return_key_used=True)
    if not isinstance(verification, Verification):
        return verification
    save_new_key = True
    # Convoluted logic a bit to handle the case if somehow a user clicks the link
    # multiple times, but it had already been clicked, and somehow now removed from the
    # table, but the user with the link probably doesn't care and wants to, in
    # frustration, "get removed from the digest"
    if verification.key_used:
        digest = EmailDigest.objects.filter(user=verification.user)
        if digest and digest[0].opted_out is False:
            digest[0].opted_out = True
            digest[0].save()
        if digest:
            save_new_key = False
    if save_new_key:
        time_used = datetime.datetime.now()
        EmailDigest(user=verification.user, opted_out_time=time_used, opted_out=True).save()
        verification.key_used = time_used
        verification.save()
    messages.success(request, '{email} successfully removed from digest list!'.format(email=verification.user.email))
    return render(request, 'users/email_opt_out_message.html', {})
